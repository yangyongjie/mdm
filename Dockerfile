FROM cr.d.xiaomi.net/containercloud/openjdk:8-jre-alpine
ENV TZ "Asia/Shanghai"
COPY mdm-redis/target/*.jar /opt/app.jar
WORKDIR /opt
CMD ["java", "-jar", "app.jar"]