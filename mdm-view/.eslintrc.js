module.exports = {
  extends: [require.resolve('@umijs/fabric/dist/eslint')],
  globals: {
    ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION: true,
    page: true,
    REACT_APP_ENV: true
  },
  rules: {
    // 禁止分号断句
    'semi': ['error', 'never'],
    // 禁止冗余逗号
    'comma-dangle': ['error', 'never'],
    // 只允许单引号
    'quotes': ['error', 'single'],
    // 强制2空格缩进
    '@typescript-eslint/indent': ['error', 2],
    // 逗号后必须空格
    'comma-spacing': ['error', { 'before': false, 'after': true }],
    // 代码块前必须空格
    'space-before-blocks': ['error', { 'functions': 'always', 'keywords': 'always', 'classes': 'always' }],
    // 关键字前后必须空格
    'keyword-spacing': ['error', { 'before': true }],
    // 操作符前后必须有空格
    'space-infix-ops': ['error', { 'int32Hint': true }],
    // 不允许连续空格
    'no-multi-spaces': 'error',
    // 禁止行尾空格
    'no-trailing-spaces': 'error',
    // 强制在大括号内使用一致的空格
    'object-curly-spacing': ['error', 'always'],
    // 禁止行尾注释
    'no-inline-comments': 'error',
    // 文件必须以空行结尾
    'eol-last': ['error', 'always'],
    // 允许for of
    'no-restricted-syntax': 'off'
  }
}
