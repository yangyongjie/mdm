import { useState, useCallback } from 'react'
import store from '@/util/store'

const cacheKey = 'redis-connections'
const initConnections: Redis.Connection[] = store.get(cacheKey, [])

const updateStore = (conns: Redis.Connection[]) => {
  const newConns = JSON.parse(JSON.stringify(conns))
  for (const conn of newConns) {
    for (const db of conn.dbs) {
      db.keyCursor = '0'
      db.keys = []
      db.open = false
    }
  }
  store.set(cacheKey, newConns)
}

export default () => {
  const [connections, setConnections] = useState(initConnections)
  const [zlibEnable, setZlibEnable] = useState<boolean>(false)
  const removeConnetion = (id: string) => setConnections((conns) => {
    return conns.filter(conn => conn.id !== id)
  })

  /**
   * 添加新链接
   */
  const addConnection = (conn: Redis.Connection) => setConnections((conns) => {
    conns.push(conn)
    // 更新缓存
    updateStore(conns)
    return [...conns]
  })

  /**
   * 修改数据库信息
   * @param connectionId 连接id
   * @param names        数据库名称列表
   */
  const updateDatabase = useCallback((connectionId: string, names: string[]) => setConnections((conns) => {
    const conn = conns.find(item => item.id === connectionId)
    for (const name of names) {
      if (!conn?.dbs.find(db => db.name === name)) {
        conn?.dbs.push({ name, keyCount: 0, open: false, keyCursor: '0' })
      }
    }
    // 更新缓存
    updateStore(conns)
    return [...conns]
  }), [])

  const updateKeys = (connectionId: string, name: string, keys: string[], cursor: string, open: boolean = false) => setConnections((conns) => {
    const conn = conns.find(item => item.id === connectionId)
    keys.sort()
    const db = conn?.dbs.find(item => item.name === name)
    if (db) {
      db.keys = keys
      db.keyCursor = cursor
    }
    for (const item of conn?.dbs || []) {
      if (item.name === name) {
        item.keys = keys
        item.open = open
      } else {
        item.open = false
      }
    }
    updateStore(conns)
    return [...conns]
  })

  const setCurrentConnection = (connectionId: string) => setConnections((conns) => {
    return conns.map(conn => ({ ...conn, show: connectionId === conn.id }))
  })

  return { connections, addConnection, updateDatabase, updateKeys, setCurrentConnection, removeConnetion, zlibEnable, setZlibEnable }
}
