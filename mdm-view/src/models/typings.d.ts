declare namespace Redis {
  interface Connection {
    // redis连接在前端的唯一标识
    id: string;
    // 连接名
    name: string;
    // 模式：单点或集群
    mode: 'single' | 'cluster';
    // 连接地址
    host: string;
    // 密码
    password: string;
    // 用户名
    username: string;
    // 是否当前显示的链接
    show: boolean;
    // 数据库，仅前端缓存用
    dbs: { name: string, open: boolean, keyCount: number, keys?: string[], keyCursor: string }[]
  }

  interface Model {
    connections: Connection[]
    addConnection: any
    updateDatabase: any
    updateKeys: any
    setCurrentConnection: any
    removeConnetion: any
    setZlibEnable: any
    zlibEnable: boolean
  }
}

declare namespace Mysql {

}
