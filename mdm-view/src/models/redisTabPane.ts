import { useState } from 'react'

export type RedisTabPane = {
  connectionId?: string,
  redisKey?: string,
  dataType?: string,
  viewType: string,
  ttl?: number,
  title: string,
  content?: string,
  currentCursor?: string,
  total?: number,
  key: string | undefined,
  closable?: boolean
}

export type OtherTabPane = {
  key: string,
  title: string,
  TabComponent: any
}

const initialRedisTabPanes: RedisTabPane[] = []
const initialOtherTabPanes: OtherTabPane[] = []

export default () => {

  const [activeKey, setActiveKey] = useState('')

  const [tabPaneList, setRedisTabPaneList] = useState(initialRedisTabPanes)
  const [otherTabPaneList, setOtherTabPaneList] = useState(initialOtherTabPanes)

  const changeActiveKey = (key: any) => setActiveKey(() => key)

  /**
   * 添加新Tab的同时打开新添加的Tab
   */
  const addRedisTabPane = (pane: RedisTabPane) => setRedisTabPaneList((panes) => {
    if (panes.find(p => p.key === pane.key)) {
      return [...panes]
    }
    return [...panes, pane ]
  })

  /**
   * 添加新Tab的同时打开新添加的Tab
   */
  const addOtherTabPane = (pane: OtherTabPane) => setOtherTabPaneList((panes) => {
    if (panes.find(p => p.key === pane.key)) {
      return [...panes]
    }
    changeActiveKey(pane.key)
    return [...panes, pane ]
  })

  /**
   * 打开一个新的搜索Tab，并启用
   * @returns panes
   */
  const openSearchRedisTabPane = () => setRedisTabPaneList((panes) => {
    const SEARCH_KEY = 'REDIS-SEARCH-TAB-KEY'
    const SEARCH_TAB_TITLE = 'Search Key'
    if (panes.find(p => p.key === SEARCH_KEY)) {
      return [...panes]
    }
    const pane: RedisTabPane = {
      title: SEARCH_TAB_TITLE,
      key: SEARCH_KEY,
      viewType: 'Plain Text'
    }
    changeActiveKey(SEARCH_KEY)
    return [...panes, pane]
  })

  /**
   * 根据key删除
   * @param tabPaneKey tabPane的key
   */
  const removeRedisTabPaneByKey = (tabPaneKey: string) => {
    let lastIndex = 0
    setRedisTabPaneList((panes) => {
      for (let i = 0;i < panes.length;i += 1) {
        if (panes[i].key === tabPaneKey) {
          panes.splice(i, 1)
          lastIndex = i - 1
          break
        }
      }
      return [...panes]
    })
    return lastIndex >= 0 ? lastIndex : 0
  }

  /**
   * 删除其他Tab
   * @param tabPaneKey
   * @returns
   */
  const removeOtherTabPaneByKey = (tabPaneKey: string) => {
    let lastIndex = 0
    setOtherTabPaneList((panes) => {
      for (let i = 0;i < panes.length;i += 1) {
        if (panes[i].key === tabPaneKey) {
          panes.splice(i, 1)
          lastIndex = i - 1
          break
        }
      }
      return [...panes]
    })
    return lastIndex
  }

  /**
   * 根据connectionId删除
   * @param connectionId 连接id
   */
  const removeRedisTabPanesByConnectionId = (connectionId: string) => {
    let lastIndex = 0
    setRedisTabPaneList((panes) => {
      for (let i = panes.length - 1;i >= 0;i -= 1) {
        if (panes[i].connectionId === connectionId) {
          panes.splice(i, 1)
          lastIndex = i - 1
        }
      }
      return [...panes]
    })
    return lastIndex >= 0 ? lastIndex : 0
  }

  return {
    activeKey,
    changeActiveKey,
    tabPaneList,
    addRedisTabPane,
    otherTabPaneList,
    addOtherTabPane,
    openSearchRedisTabPane,
    removeRedisTabPaneByKey,
    removeOtherTabPaneByKey,
    removeRedisTabPanesByConnectionId
  }
}
