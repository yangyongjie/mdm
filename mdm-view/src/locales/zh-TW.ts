import component from './zh-TW/component'
import globalHeader from './zh-TW/globalHeader'
import menu from './zh-TW/menu'
import pwa from './zh-TW/pwa'
import settingDrawer from './zh-TW/settingDrawer'
import settings from './zh-TW/settings'

export default {
  'navBar.lang': '語言',
  'layout.user.link.help': '幫助',
  'layout.user.link.privacy': '隱私',
  'layout.user.link.terms': '條款',
  'app.copyright.produced': 'WEB版多數據源數據管理工具 電視與視頻業務部南京服務端團隊研發',
  'app.preview.down.block': '下載此頁面到本地項目',
  ...globalHeader,
  ...menu,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component
}
