import { Button, Modal, Form, Input, Row, Space, message } from 'antd'
import { useModel } from 'umi'
import React, { useState } from 'react'
import { PlusOutlined } from '@ant-design/icons'
import request from '@/util/request'

export default (props: any) => {

  const { addConnection } = useModel('redis' as any, (ret: Redis.Model) => ({
    addConnection: ret.addConnection
  }))

  const [visible, setVisible] = useState(false)

  const formRef: React.Ref<any> = React.createRef()

  const save = () => {
    formRef.current.validateFields().then(async (params: any) =>{
      const response: API.CommonResult = await request('/connection', { method: 'get', params })
      if (response.success) {
        addConnection({})
        message.success('添加成功')
      }
    }).catch(() => {})
  }

  return (
    <>
      <Button {...props} onClick={() => setVisible(true)} disabled><PlusOutlined />Key</Button>
      <Modal
        visible={visible}
        onCancel={() => setVisible(false)}
        closable={false}
        footer={false}
        width={520}
        destroyOnClose
      >
        <Form labelCol={{ xs:4 }} style={{ marginTop:20 }} ref={formRef}>
          <Form.Item name="key" label="key" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="value" label="value" rules={[{ required: true }]}>
            <Input.TextArea />
          </Form.Item>
        </Form>
        <Row justify="end">
          <Space >
            <Button onClick={() => setVisible(false)}>取消</Button>
            <Button type="primary" onClick={() => save()}>保存</Button>
          </Space>
        </Row>
      </Modal>
    </>
  )
}
