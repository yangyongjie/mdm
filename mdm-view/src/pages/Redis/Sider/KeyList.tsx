import { Table, Empty, Spin } from 'antd'
import { useModel } from 'umi'
import request from '@/util/request'
import style from './KeyList.less'

export default (props: any) => {

  const { connections, zlibEnable } = useModel('redis' as any, (ret: Redis.Model) => ({
    connections: ret.connections,
    zlibEnable: ret.zlibEnable
  }))

  const { addRedisTabPane, setActiveKey } = useModel('redisTabPane' as any, (ret: any) => ({
    addRedisTabPane: ret.addRedisTabPane,
    setActiveKey: ret.changeActiveKey
  }))

  const connection = connections.find(item => item.show)


  let keys = connection?.dbs.find(item => item.open)?.keys?.map(key => ({ key }))
  // 数据筛选
  const { filterTxt } = props
  if (keys && filterTxt && filterTxt !== '') {
    keys = keys.filter((item) => item.key.includes(filterTxt))
  }

  const query = async (key: string) => {
    const { host, password, mode, dbs } = connection || {}
    const db = dbs?.find(item => item.open)
    const dbName: string = db?.name as string
    const response: API.DataResult<any> = await request('/data/obtain', {
      method: 'get',
      params: {
        host, password, mode, key, database: dbName.substr(2), node: mode === 'cluster' ? `${dbName.split(':')[0] }:${ dbName.split(':')[1].split('.')[0]}` : undefined, zlibEnable
      }
    })
    if (response.success) {
      const { ttl, dataType, cursor, total } = response.data
      const { data } = response.data
      let content: any = data
      if (dataType === 'hash') {
        content = Object.keys(data).map((item: any) => ({ key: item, value: data[item] }))
      }
      if (dataType === 'list') {
        content = data.map((item: any) => ({ key: item, value: item }))
      }
      addRedisTabPane({
        connectionId: connection?.id,
        redisKey:  key,
        dataType,
        viewType: 'Plain Text',
        ttl,
        title: connection?.name ? `${`${connection?.name }-${ key}`.substring(0, 20) }...` : key,
        content,
        key: `${connection?.id}${key}`,
        currentCursor: cursor,
        total
      })
      setActiveKey(`${connection?.id}${key}`)
    }
  }

  return (
    <Spin spinning={props.loading}>
      <div className={style['key-list']}>
        {keys && keys.length > 0 ?
          <Table
            {...props}
            rowClassName={style.row}
            pagination={{
              size: 'small',
              showSizeChanger: false,
              pageSize: 15,
              showQuickJumper: false,
              simple: true
            }}
            showHeader={false}
            size="small"
            dataSource={keys}
            columns={[{
              key: 'key',
              dataIndex: 'key',
              ellipsis: true,
              render: (key) => <span onClick={() => query(key)} className={style.key}>{key}</span>
            }]}
          /> : <Empty description="no keys" style={{ marginTop:100 }} />}
      </div>
    </Spin>
  )
}
