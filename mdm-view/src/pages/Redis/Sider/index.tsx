import React, { useState, useRef, useEffect } from 'react'
import { useModel } from 'umi'
import { Row, Input, Select, Tooltip, Dropdown, Menu, Button } from 'antd'
import { SyncOutlined, EditOutlined, CaretDownFilled, CodeOutlined, PlusOutlined, EllipsisOutlined, SearchOutlined, OrderedListOutlined } from '@ant-design/icons'
import request from '@/util/request'
import ConnectionForm from './ConnectionForm'
import AddNewKeyButton from './AddNewKeyButton'
import KeyList from './KeyList'
import style from './index.less'

export default () => {

  const connForm: React.RefObject<any> = useRef(null)
  const dbSelect: React.RefObject<any> = useRef(null)
  // 筛选keys的文本
  const [filterTxt, setFilterTxt] = useState('')

  const [keysLoading, setKeysLoading] = useState(false)

  const { connections, updateKeys, setCurrentConnection } = useModel('redis' as any, (ret: Redis.Model) => ({
    connections: ret.connections,
    updateKeys: ret.updateKeys,
    setCurrentConnection: ret.setCurrentConnection
  }))

  const { openSearchRedisTabPane, addOtherTabPane } = useModel('redisTabPane' as any, (ret: any) => ({
    openSearchRedisTabPane: ret.openSearchRedisTabPane,
    addOtherTabPane: ret.addOtherTabPane
  }))

  const menu = (
    <Menu>
      {connections.map(conn => <Menu.Item key={conn.name} onClick={() => setCurrentConnection(conn.name)}>{conn.name}</Menu.Item>)}
    </Menu>
  )

  const connection: Redis.Connection = connections.find((item: Redis.Connection) => item.show) as Redis.Connection
  const keyCursor = connection?.dbs.find(item => item.open)?.keyCursor

  const loadKeys = async (value: any, clear: boolean = false) => {
    if (!value) {
      return
    }
    const { host, password, mode } = connection
    setKeysLoading(true)
    const db = connection?.dbs.find(item => item.open)
    const offset = db?.keyCursor
    const response: API.DataResult<{data: string[], cursor: string}> = await request('/connection/keys', {
      method: 'get',
      params: {
        host,
        password,
        mode,
        database: (value as string).split('db')[1],
        cursor: offset && offset !== '0' && !clear ? offset : undefined,
        node: mode === 'cluster' ? `${value.split(':')[0] }:${ value.split(':')[1].split('.')[0]}` : undefined
      }
    })
    if (response.success) {
      const { data, cursor } = response.data
      const newKeys = Array.from(new Set([...(!clear && db?.keys ? db.keys : []), ...data]))
      updateKeys(connection.id, value, newKeys, cursor, true)
    }
    setKeysLoading(false)
  }

  useEffect(()=> {
    const init = () => loadKeys(connection?.dbs[0].name, true)
    init().then(() => {})
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connection])

  const dbName = connection?.dbs.find(db => db.open)?.name

  return (
    <div className={style.container}>
      <ConnectionForm ref={connForm}/>
      <Row justify="start" className={style.row1}>
        {/* 添加连接按钮 */}
        <Button
          type="dashed"
          className={style['connection-button']}
          onClick={() => connForm.current?.open('add')}
        ><PlusOutlined />Connection</Button>
      </Row>
      <Row justify="start" align="middle" className={style.row2}>
        {/* 数据库切换 */}
        <Dropdown overlay={menu}>
          <a className={style.name}>
            <Row justify="start" align="middle">
              <span className={style.text}>{connection?.name ? connection?.name : 'no connections'}</span>
              <CaretDownFilled className={style.down} />
            </Row>
          </a>
        </Dropdown>
        {/* 编辑 */}
        <Tooltip title="edit connection">
          <EditOutlined className={style.icon} onClick={() => connForm.current?.open('edit', connection)} />
        </Tooltip>
        <Tooltip title="refresh">
          <SyncOutlined className={style.icon} onClick={() => loadKeys(connection?.dbs.find(db => db.open)?.name, true)} />
        </Tooltip>
        {/* 返回主页 */}
        <Tooltip title="Search Key">
          <SearchOutlined className={style.icon} onClick={() => openSearchRedisTabPane()} />
        </Tooltip>
        <Dropdown
          overlay={(
            <Menu>
              <Menu.Item key="DB Info" icon={<OrderedListOutlined />} onClick={() => addOtherTabPane({ key: 'db info', title: 'db info' })}>
                db info
              </Menu.Item>
              <Menu.Item key="Terminal" icon={<CodeOutlined/>} onClick={() => addOtherTabPane({ key: 'terminal key', title: 'Terminal' })}>
                <span>terminal</span>
              </Menu.Item>
            </Menu>
          )}
        >
          <EllipsisOutlined />
        </Dropdown>
      </Row>
      {/* 数据库选择框，添加key按钮 */}
      <Row justify="start" className={style.row3}>
        <Select
          ref={dbSelect}
          className={style['db-select']}
          options={connection?.dbs.map(db => ({ label: `${db.name}`, key: db.name, value: db.name }))}
          value={dbName || connection?.dbs[0].name}
          placeholder="select db"
          onChange={async(value) => loadKeys(value, true)}
        />
        {/* 添加key */}
        <AddNewKeyButton className={style['add-new-key']} />
      </Row>
      {/* key搜索框 */}
      <Row justify="start" className={style.row4}>
        <Input.Search value={filterTxt} onChange={(e) => setFilterTxt(e.target.value)} placeholder="enter key to search" />
      </Row>
      <KeyList loading={keysLoading} filterTxt={filterTxt} className={style.keys} />
      {keyCursor && keyCursor !== '0' && filterTxt === '' ?
        <Button className={style['load-more']} onClick={() => loadKeys(connection?.dbs.find(db => db.open)?.name)}>
          Load More
        </Button>
        : <></>
      }
    </div>
  )
}
