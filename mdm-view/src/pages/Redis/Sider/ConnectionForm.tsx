import { Button, Modal, Form, Input, Radio, Row, Space, message, Spin, InputNumber } from 'antd'
import { SaveOutlined } from '@ant-design/icons'
import React, { useState, useImperativeHandle, useEffect } from 'react'

import { useModel } from 'umi'
import request from '@/util/request'

const Connection = (props: any, ref: any) => {

  const { addConnection, connections, removeConnection } = useModel('redis' as any, (ret: Redis.Model) => ({
    addConnection: ret.addConnection,
    connections: ret.connections,
    removeConnection: ret.removeConnetion
  }))

  const [connection, setConnection] = useState<Redis.Connection | undefined>(undefined)
  // 是否可见
  const [visible, setVisible] = useState(false)
  // loading动画
  const [loading, setLoading] = useState(false)
  //
  const [type, setType] = useState('add')

  const [mutiHost, setMutiHost] = useState(connection?.mode === 'cluster')

  const open = (val: 'add'|'edit', conn: any = null) => {
    if (val === 'edit') {
      setConnection(conn)
    } else {
      setConnection(undefined)
    }
    setType(val)
    setVisible(true)
  }

  useImperativeHandle(ref, () => ({
    open
  }))

  const formRef: React.Ref<any> = React.createRef()

  useEffect(() => {
    const clearHost = () => {
      if (mutiHost) {
        formRef.current.setFields([{ name: 'host', value: '' }])
      } else {
        formRef.current?.setFields([
          { name: 'host', value: '127.0.0.1' },
          { name: 'port', value: '6379' }
        ])
      }
    }
    clearHost()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mutiHost])

  const save = () => {
    formRef.current.validateFields().then(async (params: any) =>{
      const { mode, password, username, port } = params
      let { name, host } = params
      if (mode === 'single') {
        host = `${host}:${port}`
      }
      if (name === '') {
        name = host
      }
      if (type === 'add' && connections.find(conn => conn.name === name)) {
        message.error('连接名已存在')
        return
      }
      setLoading(true)
      const response: API.DataResult<{name: string, keyCount: number} []> = await request('/connection/establish', { method: 'get', params: { ...params, host } })
      if (response.success) {
        message.success('连接已保存')
        if (type === 'edit') {
          removeConnection(connection?.id)
        }
        addConnection({
          id: name,
          name,
          mode,
          host,
          password,
          username,
          show: true,
          dbs: response.data.map(item => ({ name: item.name, keyCount: item.keyCount, open: false, keys: [] }))
        })
        setVisible(false)
      }
      setLoading(false)
    }).catch(() => {})
  }

  // 测试连接
  const test = () => {
    formRef.current.validateFields().then(async (params: any) =>{
      const { mode, host, port } = params
      setLoading(true)
      const response: API.DataResult<{name: string, keyCount: number} []> = await request('/connection/ping', {
        method: 'get',
        params: { ...params, host: mode === 'single' ? `${host}:${port}` : host }
      })
      if (response.success) {
        message.success('连接成功')
      }
      setLoading(false)
    }).catch(() => {})
  }

  return (
    <>
      <Modal
        visible={visible}
        onCancel={() => setVisible(false)}
        closable={false}
        footer={false}
        width={520}
        destroyOnClose
      >
        <Spin spinning={loading}>
          <Form labelCol={{ xs:4 }} style={{ marginTop:20 }} ref={formRef}>
            <Form.Item name="name" label="连接名" initialValue={connection?.name || ''}>
              <Input placeholder="若为空，默认值为连接地址" autoComplete="off" />
            </Form.Item>
            <Form.Item name="mode" label="类型" initialValue={connection?.mode || 'single'} rules={[{ required: true }]}>
              <Radio.Group
                options={[{ label: '单点', value: 'single' }, { label: '集群', value: 'cluster' }]}
                onChange={(e) => setMutiHost(e.target.value === 'cluster')}
              />
            </Form.Item>
            {
              mutiHost || connection?.mode === 'cluster' ?
                <Form.Item
                  name="host"
                  label="地址"
                  rules={[{ required: true }]}
                  initialValue={connection?.host}
                >
                  <Input.TextArea autoSize={{ maxRows: 3, minRows:3 }} placeholder="多个节点请以英文逗号分割" autoComplete="off" />
                </Form.Item> :
                <Form.Item
                  label="地址"
                  required
                >
                  <Input.Group compact>
                    <Form.Item name="host" rules={[{ required: true }]} initialValue={connection?.host.split(':')[0] || '127.0.0.1'}>
                      <Input style={{ width: 290 }} autoComplete="off" />
                    </Form.Item>
                    <Form.Item name="port" rules={[{ required: true }]} initialValue={connection?.host.split(':')[1] || '6379'}>
                      <InputNumber style={{ width: 104 }} autoComplete="off"/>
                    </Form.Item>
                  </Input.Group>
                </Form.Item>
            }

            <Form.Item name="password" label="密码" initialValue={connection?.password || ''} >
              <Input.Password autoComplete="off" />
            </Form.Item>
            <Form.Item name="username" label="用户名" initialValue={connection?.username || ''}>
              <Input placeholder="redis版本大于6.0" autoComplete="off" />
            </Form.Item>
          </Form>
          <Row style={{ marginTop: 120 }} justify="end">

            <Space style={{ marginLeft: 100 }}>
              <Button onClick={() => setVisible(false)}>返回</Button>
              <Button style={{ marginLeft: 0 }} onClick={() => test()}>测试连接</Button>
              <Button type="primary" onClick={() => save()} icon={<SaveOutlined />}>保存</Button>
            </Space>
          </Row>
        </Spin>
      </Modal>
    </>
  )
}

export default React.forwardRef(Connection)
