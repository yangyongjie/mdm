/* eslint-disable @typescript-eslint/no-unused-vars */
import { DeleteOutlined, InfoCircleOutlined, SaveOutlined, SyncOutlined, SearchOutlined } from '@ant-design/icons'
import { Button, Col, Input, Row, Tooltip, Select, Table, Space, Form, Switch } from 'antd'
import type { TableColumnProps } from 'antd'
import React, { useState, useEffect } from 'react'
import { useModel } from 'umi'
import jsonTheme, { strToBinary, strToHex, handleParseData } from './common'
import request from '@/util/request'
import style from './index.less'
import JSONTree from 'react-json-tree'

type RedisTabProps = {
  tabKey: string
  redisKey: string
  data: string
  dataType: string
  viewType: string
  ttl: number
  currentCursor?: string
  total?: number
}

const RedisTab: React.FC<RedisTabProps> = (props) => {

  const searchFormRef: React.RefObject<any> = React.createRef()
  const [searchType, setSearchType] = useState<string>('')
  const [showTable, setShowTable] = useState<boolean>(true)

  const [redisKey, setRedisKey] = useState(props.redisKey)
  const [data, setData] = useState<any>(props.data)
  const [dataType, setDataType] = useState(props.dataType)
  const [viewType, setViewType] = useState(props.viewType)
  const [ttl, setTtl] = useState(props.ttl)
  const [currentCursor, setCurrentCursor] = useState(props.currentCursor)
  const [total, setTotal] = useState(props.total)
  const [loading, setLoading] = useState<boolean>(false)

  // 非string时文本框内容
  const [content, setContent] = useState<string>('')

  const { connections, setZlibEnable, zlibEnable } = useModel('redis' as any, (ret: Redis.Model) => ({
    connections: ret.connections,
    setZlibEnable: ret.setZlibEnable,
    zlibEnable: ret.zlibEnable
  }))

  const connection = connections.find(item => item.show)

  const currentIndex = 1

  const columns: TableColumnProps<any>[] = [
    {
      title: 'Order',
      width: 100,
      render: (_text: any, _record: any, index: number) => (currentIndex + index)
    },
    {
      title: 'Value',
      dataIndex: 'value',
      key: 'value'
    }
  ]

  const hashColumns = [
    {
      title: 'Order',
      width: 100,
      render: (_text: any, _record: any, index: number) => (currentIndex + index)
    },
    {
      title: 'Feild',
      dataIndex: 'key',
      key: 'key',
      width: 200
    },
    {
      title: 'Value',
      dataIndex: 'value',
      key: 'value',
      render: (text: any) => <div style={{ height: 20, overflow: 'hidden' }}>{text}</div>
    }
  ]

  useEffect(() => {
    const conentInit = ()=> {
      if (dataType === 'string') {
        setContent(data)
      }
    }
    conentInit()
  }, [data, dataType])

  const contentViewMap = {
    'Plain Text': (text: string) => <Input.TextArea className={style['text-viewer']} value={text} />,
    'JSON': (text: string) => <div className={style['json-viewer']}><JSONTree data={handleParseData(text)} theme={jsonTheme} shouldExpandNode={() => true}/></div>,
    'HEX': (text: string) => <Input.TextArea className={style['text-viewer']} value={strToHex(text)} />,
    'binary': (text: string) => <Input.TextArea className={style['text-viewer']} value={strToBinary(text)} />
  }

  const handleSelectChange = (value: string) => {
    if (!value) {
      return
    }
    setViewType(value)
  }

  const handleOnSearch = async () => {

    searchFormRef.current.validateFields().then(async (result: any) =>{
      const { key, arg } = result
      if (!key || key === '') {
        return
      }
      const argMap = {
        'list': 'index',
        'zset': 'value',
        'hash': 'field'
      }

      const argName = argMap[searchType]
      const { host, password, mode, dbs } = connection || {}
      if (!dbs) {
        return
      }
      const db = dbs.find(item => item.open) || dbs[0]
      const dbName = db?.name as string
      const node = mode === 'cluster' ? `${dbName.split(':')[0] }:${ dbName.split(':')[1].split('.')[0]}` : undefined
      const params = argName ?
        { host, password, mode, key, database: dbName.substr(2), [argName]: arg, node, zlibEnable } :
        { host, password, mode, key, database: dbName.substr(2), node, zlibEnable }
      setData('')
      setContent('')
      const response: API.DataResult<any> = await request('/data/obtain', {
        method: 'get',
        params
      })
      if (response.success) {
        const resData = response.data
        setTtl(resData.ttl)
        setRedisKey(key)
        setDataType(resData.dataType)
        setCurrentCursor(resData.cursor)
        setTotal(resData.total)
        if (argName && arg && arg !== '') {
          setContent(JSON.stringify(resData.data))
          setShowTable(false)
          return
        }
        setShowTable(true)
        if (resData.dataType === 'string') {
          setData(resData.data)
        } else if (resData.dataType === 'list' || resData.dataType === 'set') {
          setData(resData.data.map((val: any) => ({ key: val, value: val } as any)))
        } else if (resData.dataType === 'hash' || resData.dataType === 'zset') {
          setData (Object.keys(resData.data).map(val => ({ key: val, value: resData.data[val] } as any)))
        }
      }
    }).catch(() => {})
  }

  const handleLoadMoreData = async () => {
    setLoading(true)
    const { host, password, mode, dbs } = connection || {}
    const db = dbs?.find(item => item.open)
    const dbName = db?.name as string
    const node = mode === 'cluster' ? `${dbName.split(':')[0] }:${ dbName.split(':')[1].split('.')[0]}` : undefined
    const response: API.DataResult<any> = await request('/data/obtain', {
      method: 'get',
      params: {
        host,
        password,
        mode,
        key: redisKey,
        database: db?.name.substr(2),
        cursor:currentCursor && currentCursor !== '' ? currentCursor : undefined,
        node,
        zlibEnable
      }
    })
    if (response.success) {
      const { cursor } = response.data
      setCurrentCursor(cursor)

      if (dataType === 'hash' || dataType === 'zset') {
        const hashObj = response.data.data
        setData([...data as [], ...Object.keys(hashObj).map(key => ({ key, value: hashObj[key] } as any))])
      }
      if (dataType === 'list' || data === 'set') {
        const listObj = response.data.data
        setData([...data as [], ...listObj.map((val: any) => ({ key: val, value: val } as any))])
      }
    }
    setLoading(false)
  }

  const rowStyle = { paddingBottom: 12 }

  const getPlaceholderByType = (): string => {
    switch (searchType) {
      case 'list': return 'Input index'
      case 'hash': return 'Input field'
      case 'zset': return 'Input value'
      default: return ''
    }
  }

  const searchForm = (
    <Form layout="inline" ref={searchFormRef}>
      <Form.Item name="type" >
        <Select
          placeholder="Select type"
          options={[
            { key:'string', value: 'string', label: 'STRING' },
            { key:'list', value: 'list', label: 'LIST' },
            { key:'hash', value: 'hash', label: 'HASH' },
            { key:'set', value: 'set', label: 'SET' },
            { key:'zset', value: 'zset', label: 'ZSET' }
          ]}
          value={searchType as string}
          onChange={(val) => setSearchType(val)}
          style={{ width: 120, fontWeight: 600 }}
          allowClear
        />
      </Form.Item>
      <Form.Item name="key">
        <Input
          style={{ width: 300 }}
          placeholder="Input search key"
          allowClear
        />
      </Form.Item>
      {
        searchType === 'list' || searchType === 'hash' || searchType === 'zset' ? <Form.Item name="arg">
          <Input
            placeholder={getPlaceholderByType()}
            allowClear
          />
        </Form.Item> : <></>
      }
      <Button onClick={() => handleOnSearch()}><SearchOutlined/></Button>
    </Form>
  )

  return (
    <div className={style.tab}>
      <Row style={rowStyle} className={style.row1}>
        <Space>
          {
            props.tabKey === 'REDIS-SEARCH-TAB-KEY' ?
              searchForm : <>
                <Input
                  readOnly
                  className={style.key}
                  placeholder="Enter your String key"
                  prefix={<span className={style.prefix}>{props.dataType.toUpperCase()}</span>}
                  suffix={
                    <Tooltip title="Redis Key">
                      <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                    </Tooltip>
                  }
                  defaultValue={props.redisKey}
                />
                <Input
                  readOnly
                  className={style.ttl}
                  placeholder="Enter The TTL"
                  prefix={<span className={style.prefix}>TTL</span>}
                  suffix={
                    <Tooltip title="Key's Valid Time">
                      <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                    </Tooltip>
                  }
                  defaultValue={ttl}
                />
                <Button icon={<DeleteOutlined/>} type="primary" danger disabled>Delete</Button>
                <Button icon={<SyncOutlined/>}>Reload</Button>
              </>
          }
        </Space>
      </Row>
      {
        dataType && dataType !== 'string' && showTable ? <Row gutter={20} style={rowStyle}>
          <Col>
            <Table
              onRow={record => {
                return {
                  onClick: () => {setContent(record.value )}
                }
              }}
              loading={loading}
              size="small"
              dataSource={data}
              columns={dataType === 'hash' || dataType === 'zset' ? hashColumns : columns}
              pagination={false}
              scroll={{ scrollToFirstRowOnChange: false, y: 200 }}
              className={style.table}
            />
            {
              currentCursor && currentCursor !== '0' ?
                <Button onClick={handleLoadMoreData} className={style['load-more']} disabled={loading}>
                  Load More
                </Button> : <></>
            }
          </Col>
        </Row> : <></>
      }
      <Row gutter={20} style={rowStyle} align="middle">
        <Col>
          <Select defaultValue={viewType} style={{ width: 120 }} onChange={handleSelectChange}>
            <Select.Option value="Plain Text">Plain Text</Select.Option>
            <Select.Option value="JSON">JSON</Select.Option>
            <Select.Option value="HEX">HEX</Select.Option>
            <Select.Option value="binary">binary</Select.Option>
          </Select>
        </Col>
        <Col>
          {/* <Switch
            checked={zlibEnable}
            checkedChildren="Zlib解压"
            unCheckedChildren="Zlib解压"
            onChange={(e) => {
              console.log(e)
              setZlibEnable(e)
            }}/> */}
        </Col>
      </Row>
      <Row gutter={20} style={rowStyle}>
        <div className={style['text-container']}>
          { contentViewMap[viewType](content) }
        </div>
      </Row>
      <Row gutter={20} style={rowStyle}>
        <Col>
          <Button type="primary" disabled icon={<SaveOutlined />}>Save</Button>
        </Col>
      </Row>
    </div>
  )
}

export default RedisTab
