export default {
  scheme: 'bright',
  author: 'chris kempson (http://chriskempson.com)',
  base00: '#000000',
  base01: '#303030',
  base02: '#505050',
  base03: '#b0b0b0',
  base04: '#d0d0d0',
  base05: '#e0e0e0',
  base06: '#f5f5f5',
  base07: '#ffffff',
  base08: '#fb0120',
  base09: '#fc6d24',
  base0A: '#fda331',
  base0B: '#a1c659',
  base0C: '#76c7b7',
  base0D: '#6fb3d2',
  base0E: '#d381c3',
  base0F: '#be643c'
}

export const strToBinary = (str: string) => {
  const result = []
  for (let i = 0;i < str.length;i += 1) {
    const binaryStr = str.charCodeAt(i).toString(2)
    result.push(`0${binaryStr}`)
  }
  return result.join('')
}

export const strToHex = (str: string) => {
  const result = []
  for (let i = 0;i < str.length;i += 1) {
    const binaryStr = str.charCodeAt(i).toString(16)
    result.push(binaryStr)
  }
  return result.join('')
}

export const handleParseData = (dataValue: string) => {
  let data = {}
  try {
    data = JSON.parse(dataValue)
  } catch {
    data = { value: dataValue, 'error': 'This value is not a correct json string' }
  }
  return data
}


