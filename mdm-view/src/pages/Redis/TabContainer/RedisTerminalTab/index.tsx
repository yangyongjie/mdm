import request from '@/util/request'
import { Col, Input, Row } from 'antd'
import Title from 'antd/lib/typography/Title'
import React, { useState } from 'react'
import { useModel } from 'umi'
import style from './index.less'

const RedisTerminalTab: React.FC = () => {

  const resultRef: React.RefObject<any> = React.createRef()

  const rowStyle = { paddingBottom: 12 }

  const { connections } = useModel('redis' as any, (ret: Redis.Model) => ({
    connections: ret.connections
  }))

  const connection = connections.find(item => item.show)

  const { host, password, mode, dbs } = connection || {}
  const db = dbs?.find(item => item.open)

  const [currentIndex, setCurrentIndex] = useState(0)
  const [command, setCommand] = useState('')
  const [commandsHistory, setCommandsHistory] = useState<any[]>([])
  const [viewData, setViewData] = useState('> ')

  const query = async (comm: string) => {

    const dbName = db?.name as string
    const node = mode === 'cluster' ? `${dbName.split(':')[0] }:${ dbName.split(':')[1].split('.')[0]}` : undefined
    const response: API.DataResult<any> = await request('/terminal/cmd', {
      method: 'get',
      params: { host, password, mode, database: dbName.substr(2), command: comm, node }
    })
    let res: string = ''
    if (response.success) {
      if (typeof response.data === 'undefined') {
        res = ''
      } else if (response.data instanceof Array) {
        res = response.data.map(value => window.atob(value)).join('\r\n')
      } else {
        try {
          res = window.atob(response.data)
        } catch {
          res = response.data
        }
      }
      const textarea: any = document.getElementById('textarea')
      textarea.scrollTop = textarea.scrollHeight
      commandsHistory.push(comm)
      setCommandsHistory([...commandsHistory])
      setCurrentIndex(commandsHistory.length)
      setViewData((data) => {
        return `${data + res }\n> `
      })
    }
  }

  const handleCommandOnchange = (e: any) => {
    setCommand(e.target.value)
  }


  const handleCommandExcute = async () => {
    setViewData((preValue) => `${preValue + command }\n`)
    query(command).then(() => {
      setCommand('')
    })
  }

  const handleCommandHistory = (e: any) => {
    if (e.key === 'ArrowUp') {
      if (currentIndex > 0 && currentIndex <= commandsHistory.length) {
        const index = currentIndex - 1
        if (index >= 0) {
          setCurrentIndex(index)
          setCommand(commandsHistory[index])
        } else {
          setCommand(commandsHistory[0])
        }
      }
    } else if (e.key === 'ArrowDown') {
      if (currentIndex >= 0 && currentIndex < commandsHistory.length) {
        const index = currentIndex + 1
        if (index < commandsHistory.length) {
          setCurrentIndex(index)
          setCommand(commandsHistory[index])
        } else {
          setCommand(commandsHistory[commandsHistory.length - 1])
        }
      }
    } else if (e.key === 'Backspace') {
      setCurrentIndex(commandsHistory.length)
    }
  }


  return (
    <Row style={rowStyle}>
      <Col>
        <Title level={3} style={{ marginLeft: '10px' }}>{ host } connected!</Title>
        <div className={style['text-container']}>
          <Input.TextArea readOnly className={style['text-viewer']} value={viewData} ref={resultRef} id="textarea"/>
          <Input onKeyDown={(e) => handleCommandHistory(e)} className={style['command-viewer']} placeholder="Press Enter To Exec Commands, Up and Down To Switch History" onPressEnter={() => handleCommandExcute()} value={command} onChange={(e) => {handleCommandOnchange(e)}}/>
        </div>
      </Col>
    </Row>

  )

}

export default RedisTerminalTab
