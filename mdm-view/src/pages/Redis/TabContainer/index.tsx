import { useModel } from 'umi'
import { Tabs } from 'antd'
import RedisTab from './RedisTab'
import style from './index.less'
import RedisTerminalTab from './RedisTerminalTab'
import RedisInfoTab from './RedisInfoTab'

const { TabPane } = Tabs

const TabContainer = (props: any) => {

  const { paneList, otherTabPaneList, removeOtherTabPaneByKey, removeRedisTabPaneByKey, activeKey, setActiveKey } = useModel('redisTabPane' as any, (ret) => ({
    activeKey: ret.activeKey,
    setActiveKey: ret.changeActiveKey,
    paneList: ret.tabPaneList,
    otherTabPaneList: ret.otherTabPaneList,
    removeOtherTabPaneByKey: ret.removeOtherTabPaneByKey,
    removeRedisTabPaneByKey: ret.removeRedisTabPaneByKey
  }))

  const onChange = (key: string) => {
    setActiveKey(key)
  }

  const remove = (targetKey: string) => {
    let lastIndex = removeRedisTabPaneByKey(targetKey)
    if (targetKey === 'terminal key' || targetKey === 'db info') {
      const index = removeOtherTabPaneByKey(targetKey)
      if (index >= 0) {
        lastIndex = index
      }
    }
    if (targetKey === activeKey) {
      if (paneList.length > 0) {
        setActiveKey(paneList[lastIndex].key)
      } else {
        setActiveKey('')
      }
    }
  }

  const onEdit = (targetKey: any, action: 'add' | 'remove') => {
    if (action === 'remove') {
      remove(targetKey)
    }
  }

  return (
    <>
      <div className={style['tab-container']} {...props}>
        <Tabs
          type="editable-card"
          hideAdd
          onChange={onChange}
          activeKey={activeKey}
          onEdit={onEdit}
        >
          {paneList.map((pane: any) => (
            <TabPane style={{ margin: 24 }} tab={pane.title} key={pane.key} closable={pane.closable}>
              <RedisTab tabKey={pane.key} redisKey={pane.redisKey} data={pane.content} dataType={pane.dataType} viewType={pane.viewType} ttl={pane.ttl} currentCursor={pane.currentCursor} total={pane.total}/>
            </TabPane>
          ))}
          {otherTabPaneList.map((pane: any) => (
            <TabPane style={{ margin: 24 }} tab={pane.title} key={pane.key}>
              {
                pane.key === 'terminal key' ? <RedisTerminalTab/> : <RedisInfoTab/>
              }
            </TabPane>
          ))}
        </Tabs>
      </div>
    </>

  )
}

export default TabContainer
