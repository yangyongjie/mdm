import { useEffect, useState } from 'react'
import { useModel } from 'umi'
import { Row, Descriptions } from 'antd'
import request from '@/util/request'

export default () => {

  const { connections } = useModel('redis' as any, (ret: Redis.Model) => ({
    connections: ret.connections
  }))

  const [info, setInfo] = useState<any>({})

  useEffect(()=> {
    const conn = connections.find(item => item.show)
    if (!conn) {
      return
    }
    const { host, password, mode } = conn
    const init = async() => {
      const response: API.DataResult<any> = await request('/server/info', {
        params: { host, password, mode }
      })
      if (response.success) {
        setInfo(response.data[0])
      }
    }
    init()
  }, [connections])


  return (
    <div>
      {
        Object.keys(info === null ? {} : info).map((title: string) => (
          <Row style={{ marginBottom: 30 }}>
            <Descriptions title={title} bordered column={{ xs: 1, md: 2, lg: 3 }}>{
              Object.keys(info[title]).map((key: string) => (<Descriptions.Item span={1} label={key}>{info[title][key]}</Descriptions.Item>))
            }</Descriptions>
          </Row>
        ))
      }
    </div>
  )
}
