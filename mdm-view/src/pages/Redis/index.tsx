import React from 'react'
import { Row } from 'antd'
import Sider from './Sider'
import TabContainer from './TabContainer'
import style from './index.less'

export default class Redis extends React.Component {

  render() {
    return (
      <Row className={style['redis-container']}>
        <Sider />
        <TabContainer/>
      </Row>)
  }
}
