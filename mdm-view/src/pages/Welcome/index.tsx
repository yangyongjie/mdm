import React from 'react'
import DocumentTitle from 'react-document-title'
import { enquireScreen } from 'enquire-js'
import Banner from './Banner'
import Content from './Content'
import './static/style'

let isMobile = false
enquireScreen((b: any) => {
  isMobile = b
})

class Home extends React.PureComponent {
  state = {
    isMobile,
    showShadow: false
  };

  componentDidMount() {
    enquireScreen((b: any) => {
      this.setState({
        isMobile: !!b
      })
    })
  }
  navToShadow = (e: any) => {
    this.setState({ showShadow: e.mode === 'leave' })
  }
  render() {
    return (
      [
        <Banner key="banner" isMobile={this.state.isMobile} navToShadow={this.navToShadow} />,
        <Content key="content" />,
        <DocumentTitle key="mdm" title="MDM" />
      ]
    )
  }
}
export default Home
