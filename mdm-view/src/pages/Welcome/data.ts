export const content = [
  {
    img: '/redis.svg',
    href: '/redis',
    title: 'Redis',
    description: ' 是一个由 Salvatore Sanfilippo 写的 key-value 存储系统，是跨平台的非关系型数据库。'
  },
  {
    img: '/mysql.svg',
    title: 'MySQL',
    href: '/mysql',
    description: ' 在 WEB 应用方面，是最好的 RDBMS 应用软件之一。'
  },
  {
    img: '/mongo.svg',
    title: 'MongoDB',
    href: '/mongo',
    description: ' 是一个基于分布式文件存储的数据库，旨在为 WEB 应用提供可扩展的高性能数据存储解决方案。'
  }
]
