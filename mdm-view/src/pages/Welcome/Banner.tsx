import React from 'react'
import QueueAnim from 'rc-queue-anim'
import { Element } from 'rc-scroll-anim'
import { Row } from 'antd'

class Banner extends React.PureComponent<any> {

  static defaultProps = {
    className: 'banner'
  }

  render() {
    const { className, isMobile, navToShadow } = this.props
    return (
      <Element component="section" className={`${className}-wrapper page`} onChange={navToShadow}>
        <div className={className}>
          <QueueAnim className={`${className}-img-wrapper`} delay={300} type="right">
            <Row justify="center" align="middle" style={{ height: '100%' }}>
              <img width="40%" src="/logo.svg" alt="" />
            </Row>
          </QueueAnim>
          <QueueAnim
            type={isMobile ? 'bottom' : 'right'}
            className={`${className}-text-wrapper`}
            delay={300}
          >
            <h1 key="h1">MDM数据管理工具</h1>
            <p className="main-info" key="p">
              MDM 是全新一代多数据源、可视化、跨平台的数据管理工具，致力于提供一套简单方便、安全可靠的集成方案。
            </p>
          </QueueAnim>
        </div>
      </Element>
    )
  }
}

export default Banner
