import { Button, Result } from 'antd'
import React from 'react'
import { history } from 'umi'

const NoFoundPage: React.FC = () => (
  <Result
    status="warning"
    title="提示"
    subTitle="抱歉，此功能开发中，尽请期待！"
    extra={
      <Button type="primary" onClick={() => history.push('/')}>
        返回主页
      </Button>
    }
  />
)

export default NoFoundPage
