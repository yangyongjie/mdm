const prefix = 'mdm-cache:'

const set = (key: string, value: any) => localStorage.setItem(`${prefix}${key}`, JSON.stringify(value))
const get = (key: string, defaultValue: any = null) => {
  const value = localStorage.getItem(`${prefix}${key}`)
  return value ? JSON.parse(value) : defaultValue
}

export default {
  set, get
}
