declare namespace API {

  class CommonResult {
    code: number;
    message: number;
    success: boolean;
  };

  interface DataResult<T> extends CommonResult {
    data: T;
  }

}
