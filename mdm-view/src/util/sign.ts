import { KJUR, KEYUTIL, hextob64 } from 'jsrsasign'
import { Base64 } from 'js-base64'

// 私钥
const priKey = '-----BEGIN PRIVATE KEY-----\n' +
 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAIpwC7CAZlIhHqDSgdv/sso+3+t' +
  '3q5a3xJQlYfo/gNjLsL//YEXtUf4Z21hc/ckwmwcbfO2j2J0+sXYzH78M1wV83Zhm06nnJNEZi' +
  'ibmbvGc9FUremWtSNzQMivCR7JRzXh8qX9oodV/SwxZA/XAzyswe4CJteoM3nrde4+3fA/ZAgM' +
  'BAAECgYA4T69RkkkMq1AsFrSf6nitb7nhrAHuAr5BjJs4EdC1XtV8o8Jjb09kztu8K8dHqS/GS' +
  'lYczAL2o2PIMq99JRnpFmQRuGCmHVFFtU0ybt1Ujn59PsfO5hIzrH5ROSRjqhRb7zzCRtoOaye' +
  'DIp2jmaWFJfqnNXTUWOUPPzQyiOTPyQJBAL0NMEVFbZBhBAnUpb7KtrWP6vO7X9PYypH+yurn4' +
  'E6JI+jhJgvcXYMZBwUnIViJLvTI5be4hPEjvWMpF2uzdWsCQQC7dl3ouIXrGb018J+2HZ59a96' +
  'TgOTL7NtOcwLjukYl/dfk7B2ifKBdPsVenFVE070qKg5iGAkwnMICCe4xUtzLAkEAkyhe+Kzwe' +
  'kBqalSSqA8XZgEe/JZQI5FPLZHN1kike72YrEAF45mnWNL0efhZppcya+ytk/MX2LTfSP7Fclq' +
  'A/QJAVPcpSTmZOR3JbJFLUjfKhf5GO4TILsWEAOKQBLeA4+dR2yhJpkPmS10QB/nDjBaMPnghX' +
  'MSYJbhnFpV5qFFuUQJBALZYYaru4FQ5i6ilRVQhckJBDyyDl5jh5JtI+cCFJ09kt2iCQX1W/aN' +
  'Z0mhfz+MXYRyq8PAIK9qlg+VjhjzcPxM=' +
  '-----END PRIVATE KEY-----\n'

const key = KEYUTIL.getKey(priKey)

const signData = (content: any): {sign: string, data: string} => {
  const signature = new KJUR.crypto.Signature({ alg: 'SHA1withRSA' })
  const data = Base64.encode(JSON.stringify(content))
  // 填入私钥和data
  signature.init(key)
  signature.updateString(data)
  // 默认的签名是16进制，需通过base转成字符串
  const sign = hextob64(signature.sign())
  return ({
    sign, data
  })
}

export default signData
