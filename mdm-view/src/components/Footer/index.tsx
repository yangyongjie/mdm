import { useIntl } from 'umi'
import { GitlabOutlined } from '@ant-design/icons'
import { DefaultFooter } from '@ant-design/pro-layout'

export default () => {
  const intl = useIntl()
  const defaultMessage = intl.formatMessage({
    id: 'app.copyright.produced',
    defaultMessage: 'WEB版多数据源数据管理工具 电视与视频业务部南京服务端团队研发'
  })

  const currentYear = new Date().getFullYear()

  return (
    <DefaultFooter
      copyright={`${currentYear} ${defaultMessage}`}
      links={[
        {
          key: 'Ant Design Pro',
          title: 'Ant Design Pro',
          href: 'https://pro.ant.design',
          blankTarget: true
        },
        {
          key: 'Gitlab',
          title: <GitlabOutlined />,
          href: 'https://git.n.xiaomi.com/nj-tv-server/mdm',
          blankTarget: true
        },
        {
          key: 'Spring Boot',
          title: 'Spring Boot',
          href: 'https://spring.io/projects/spring-boot',
          blankTarget: true
        }
      ]}
    />
  )
}
