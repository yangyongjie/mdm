import type { Settings as LayoutSettings } from '@ant-design/pro-layout'

const Settings: LayoutSettings & {
  pwa?: boolean;
  logo?: string;
} = {
  navTheme: 'light',
  // 拂晓蓝
  primaryColor: '#1890ff',
  layout: 'top',
  contentWidth: 'Fixed',
  fixedHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  title: 'MDM',
  pwa: false,
  logo: '/logo.svg',
  iconfontUrl: '',
  menuRender: false,
  menuHeaderRender: false,
  headerRender: false,
  footerRender: false
}

export default Settings
