﻿export default [
  {
    path: '/redis',
    component: './Redis'
  },
  {
    path: '/welcome',
    component: './Welcome'
  },
  {
    path: '/',
    redirect: '/welcome'
  },
  {
    component: './404'
  }
]
