package com.xiaomi.mitv.mdm.redis.controller;

import com.xiaomi.mitv.mdm.common.constants.CommonConstant;
import com.xiaomi.mitv.mdm.common.result.HandleResult;
import com.xiaomi.mitv.mdm.common.result.ResponseEnum;
import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.redis.common.cache.RdmCache;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import io.lettuce.core.AbstractRedisAsyncCommands;
import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.ScanCursor;
import io.lettuce.core.cluster.api.async.RedisAdvancedClusterAsyncCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.RedisConnectionCommands;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceClusterConnection;
import org.springframework.data.redis.connection.lettuce.LettuceConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@RequestMapping("rdm/connection")
@RestController
public class RedisConnectController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisConnectController.class);

    /**
     * 连接测试
     */
    @GetMapping("ping")
    public HandleResult ping() {
        RdmContext rdmContext = RdmContext.currentContext();
        RedisTemplate<String, Object> redisTemplate = rdmContext.getRedisTemplate();
        String result = redisTemplate.execute(RedisConnectionCommands::ping);
        // 不管连接成功或失败，清除缓存中当前RedisTemplate
        RdmCache.REDIS_TEMPLATE_CACHE.remove(rdmContext.getToken());
        return StringUtil.equals("PONG", result) ? new HandleResult() : new HandleResult(ResponseEnum.CONNECT_FAIL);
    }

    /**
     * 建立连接
     */
    @GetMapping("establish")
    public HandleResult establish() {
        // 获取数据库信息
        RedisTemplate<String, Object> redisTemplate = RdmContext.currentContext().getRedisTemplate();
        List<Map<String, Object>> data = redisTemplate.execute((RedisCallback<List<Map<String, Object>>>) connection -> {
            List<Map<String, Object>> dbInfo = new ArrayList<>();
            // 获取数据库信息，只会返回
            Properties keyspaceProp = connection.serverCommands().info("keyspace");
            // 记录返回的数据库
            List<String> dbs = new ArrayList<>();
            for (Map.Entry<Object, Object> entry : keyspaceProp.entrySet()) {
                Map<String, Object> dbMap = new HashMap<>(4);
                String dbName = String.valueOf(entry.getKey());
                dbMap.put("name", dbName);
                String info = String.valueOf(entry.getValue());
                dbMap.put("keyCount", Integer.parseInt(info.substring(info.indexOf("keys=") + 5, info.indexOf(","))));
                dbInfo.add(dbMap);
                dbs.add(dbName);
            }
            // 判断是单机还是集群，集群环境下支持db0  Cluster
            Properties clusterProp = connection.serverCommands().info("cluster");
            // 0:单机，3：集群
            String clusterEnabled = clusterProp.getProperty("cluster_enabled");
            if (StringUtil.equals(CommonConstant.STR_ZERO, clusterEnabled)) {
                // 获取数据库数量
                byte[] countArr = (byte[]) connection.execute("config", "get".getBytes(StandardCharsets.UTF_8), "databases".getBytes(StandardCharsets.UTF_8));
                int count = Integer.parseInt(new String(countArr));
                int currSize = dbInfo.size();
                if (count > currSize) {
                    for (int i = 0; i < count; i++) {
                        String dbName = "db" + i;
                        if (dbs.contains(dbName)) {
                            continue;
                        }
                        Map<String, Object> dbMap = new HashMap<>(4);
                        dbMap.put("name", dbName);
                        dbMap.put("keyCount", 0);
                        dbInfo.add(dbMap);
                    }
                    // 按照dbName从小到大排序
                    dbInfo.sort(Comparator.comparingInt(o -> Integer.parseInt(String.valueOf(o.get("name")).replace("db", ""))));
                }
            }
            return dbInfo;
        });
        return new HandleResult(data);
    }

    /**
     * 获取当前数据库下的keys
     */
    @GetMapping("keys")
    public HandleResult keys() throws ExecutionException, InterruptedException {
        HandleResult result = new HandleResult();

        RdmContext rdmContext = RdmContext.currentContext();
        Map<String, String> data = rdmContext.getData();
        // 游标id
        String cursor = data.get("cursor");
        // 单点或集群
        String mode = rdmContext.getMode();
        RedisTemplate<String, Object> redisTemplate = rdmContext.getRedisTemplate();
        RedisConnectionFactory redisConnectionFactory = redisTemplate.getConnectionFactory();
        AbstractRedisAsyncCommands<byte[], byte[]> commands = null;
        LettuceConnection connection = null;
        try {
            if (StringUtil.equals(CommonConstant.STANDALONE, mode)) {
                // 单点
                connection = (LettuceConnection) redisConnectionFactory.getConnection();
                commands = (AbstractRedisAsyncCommands<byte[], byte[]>) connection.getNativeConnection();
            } else if (StringUtil.equals(CommonConstant.CLUSTER, mode)) {
                // 集群
                connection = (LettuceClusterConnection) redisConnectionFactory.getClusterConnection();
                // RedisAdvancedClusterAsyncCommands
                RedisAdvancedClusterAsyncCommands<byte[], byte[]> redisClusterAsyncCommands = (RedisAdvancedClusterAsyncCommands<byte[], byte[]>) connection.getNativeConnection();
                // 获取当前的节点信息
                String currentNode = data.get("node");
                String[] node = currentNode.split(":");
                commands = (AbstractRedisAsyncCommands<byte[], byte[]>) redisClusterAsyncCommands.getConnection(node[0], Integer.parseInt(node[1]));
            }

            ScanArgs scanArgs = new ScanArgs().limit(1000);

            ScanCursor scanCursor = cursor == null ? ScanCursor.INITIAL : ScanCursor.of(cursor);
            RedisFuture<KeyScanCursor<byte[]>> future = commands.scan(scanCursor, scanArgs);
            KeyScanCursor<byte[]> keyScanCursor = future.get();

            if (keyScanCursor == null) {
                result.fail(ResponseEnum.UNKNOWN_ERROR);
                return result;
            }

            List<String> keys = keyScanCursor.getKeys().stream().map(String::new).collect(Collectors.toList());
            Map<String, Object> map = new HashMap<>(4);
            map.put("cursor", keyScanCursor.getCursor());
            map.put("data", keys);
            result.setData(map);
            return result;
        } finally {
            // 释放连接到连接池中
            if (connection != null) {
                connection.close();
            }
        }
    }
}
