package com.xiaomi.mitv.mdm.redis.controller;

import com.xiaomi.mitv.mdm.common.result.HandleResult;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 终端命令接口
 */
@RestController
@RequestMapping("rdm/terminal")
public class RedisTerminalController {

    @GetMapping("cmd")
    public HandleResult cmd() {
        RdmContext rdmContext = RdmContext.currentContext();
        RedisTemplate<String, Object> redisTemplate = rdmContext.getRedisTemplate();
        // 请求参数
        Map<String, String> dataMap = rdmContext.getData();

        String commands = dataMap.get("command");
        String[] list = commands.split("\\s+");
        int length = list.length;
        if (length == 0) {
            return new HandleResult(commands);
        }
        String command = list[0];
        Object result = redisTemplate.execute((RedisCallback<Object>) connection -> {
            byte[][] byteList = new byte[length - 1][];
            // 参数转换为byte数组
            for (int i = 1; i < length; i++) {
                byteList[i - 1] = (list[i].getBytes(StandardCharsets.UTF_8));
            }
            try {
                Object obj = connection.execute(command, byteList);
                return obj instanceof String ? ((String) obj).getBytes() : obj;
            } catch (Exception e) {
                return e.getCause() == null ? e.getMessage().getBytes() : e.getCause().getMessage().getBytes();
            }
        });
        return new HandleResult(result);
    }

}
