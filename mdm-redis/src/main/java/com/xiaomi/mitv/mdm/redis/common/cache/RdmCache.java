package com.xiaomi.mitv.mdm.redis.common.cache;

import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RdmCache {

    private RdmCache() {
    }

    /**
     * 缓存客户端redisTemplate对象，每个客户端（粒度到数据库）连接对应一个redisTemplate
     * redisTemplate对象的TTL为30分钟，30分钟后自动销毁
     * 或者设置redisTemplate的长度，使用LRU算法清除不常使用的redisTemplate
     */
    public static final Map<String, RedisTemplate<String, Object>> REDIS_TEMPLATE_CACHE = new ConcurrentHashMap<>();
}
