package com.xiaomi.mitv.mdm.redis.processor;

import com.xiaomi.mitv.mdm.redis.vo.BaseDataVo;

/**
 * 抽象的redis数据处理器
 */
public abstract class AbstractRedisDataProcessor {
    /**
     * 数据查询处理
     *
     * @param key
     */
    public abstract BaseDataVo process(String key);

}
