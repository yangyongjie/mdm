package com.xiaomi.mitv.mdm.redis.handler;

/**
 * 处理器接口
 */
public interface RdmHandler {

    /**
     * 是否执行
     *
     * @return true:执行，false：不执行
     */
    boolean boolHandle();

    /**
     * 执行，并处理异常
     */
    void handle();
}
