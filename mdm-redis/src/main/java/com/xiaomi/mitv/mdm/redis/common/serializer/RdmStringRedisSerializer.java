package com.xiaomi.mitv.mdm.redis.common.serializer;

import com.xiaomi.mitv.mdm.common.util.ZlibUtil;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * 自定义字符串序列化器
 */
public class RdmStringRedisSerializer extends StringRedisSerializer {

    @Override
    public String deserialize(byte[] bytes) throws SerializationException {
        return super.deserialize(ZlibUtil.decompress(bytes));
    }
}
