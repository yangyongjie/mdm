package com.xiaomi.mitv.mdm.redis.common.context;

import com.xiaomi.mitv.mdm.common.result.HandleResult;
import com.xiaomi.mitv.mdm.redis.common.model.RedisProperties;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;

public class RdmContext {

    /**
     * 保存RdmContext的线程本地变量
     */
    private static final ThreadLocal<RdmContext> RDM_CONTEXT_THREAD_LOCAL = ThreadLocal.withInitial(RdmContext::new);

    public static RdmContext currentContext() {
        return RDM_CONTEXT_THREAD_LOCAL.get();
    }

    public static void remove() {
        RDM_CONTEXT_THREAD_LOCAL.remove();
    }

    /**
     * redis操作模板对象
     */
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 请求参数
     */
    private Map<String, String> data;

    /**
     * 连接信息
     */
    private RedisProperties redisProperties;

    /**
     * 连接信息token
     */
    private String token;

    /**
     * 处理器执行结果
     */
    private HandleResult handleResult = new HandleResult();

    /**
     * 集群模式
     */
    private String mode;

    public RedisTemplate<String, Object> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public HandleResult getHandleResult() {
        return handleResult;
    }

    public void setHandleResult(HandleResult handleResult) {
        this.handleResult = handleResult;
    }

    public RedisProperties getRedisProperties() {
        return redisProperties;
    }

    public void setRedisProperties(RedisProperties redisProperties) {
        this.redisProperties = redisProperties;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
