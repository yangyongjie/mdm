package com.xiaomi.mitv.mdm.redis.common.aspect;

import com.xiaomi.mitv.mdm.common.result.HandleResult;
import com.xiaomi.mitv.mdm.common.result.ResponseEnum;
import com.xiaomi.mitv.mdm.common.util.MDCUtil;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.handler.HandlerRunner;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RdmAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(RdmAspect.class);

    /**
     * 切点
     */
    @Pointcut(value = "@annotation(org.springframework.web.bind.annotation.RequestMapping)||@annotation(org.springframework.web.bind.annotation.PostMapping)||@annotation(org.springframework.web.bind.annotation.GetMapping)")
    private void webPointcut() {
        // doNothing
    }

    @Around(value = "webPointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 执行开始的时间
        long beginTime = System.currentTimeMillis();
        // 设置MDC参数用于日志打印
        MDCUtil.init();
        // 拦截方法执行结果
        Object result;
        try {
            // 执行前置处理器
            HandleResult handeResult = HandlerRunner.run();
            if (!handeResult.isSuccess()) {
                return handeResult;
            }
            // 执行拦截的方法
            result = joinPoint.proceed();
        } catch (RedisConnectionFailureException rcfe) {
            LOGGER.error(rcfe.getMessage(), rcfe);
            return new HandleResult(ResponseEnum.CONNECT_FAIL);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new HandleResult(ResponseEnum.UNKNOWN_ERROR);
        } finally {
            long endTime = System.currentTimeMillis();
            LOGGER.info("耗时{}毫秒", endTime - beginTime);
            // 清除MDC
            MDCUtil.remove();
            // 清除共享副本
            RdmContext.remove();
        }
        return result;
    }

}
