package com.xiaomi.mitv.mdm.redis.processor;

import com.xiaomi.mitv.mdm.common.constants.CommonConstant;
import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.redis.common.annotation.RedisDataType;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.common.util.RedisUtil;
import com.xiaomi.mitv.mdm.redis.vo.BaseDataVo;
import com.xiaomi.mitv.mdm.redis.vo.HashDataVo;
import io.lettuce.core.AbstractRedisAsyncCommands;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.ScanCursor;
import io.lettuce.core.ScoredValue;
import io.lettuce.core.ScoredValueScanCursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnection;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RedisDataType("zset")
public class RedisZsetProcessor extends AbstractRedisDataProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisZsetProcessor.class);

    @Override
    public BaseDataVo process(String key) {
        RdmContext rdmContext = RdmContext.currentContext();
        // 判断 member 参数是否为空
        Map<String, String> data = rdmContext.getData();
        String value = data.get("value");
        if (StringUtil.isNotEmpty(value)) {
            return zscore(key, value);
        }
        HashDataVo dataVo = new HashDataVo();
        RedisTemplate<String, Object> redisTemplate = rdmContext.getRedisTemplate();
        LettuceConnection connection = null;
        try {
            RedisConnectionFactory redisConnectionFactory = redisTemplate.getConnectionFactory();
            connection = (LettuceConnection) redisConnectionFactory.getConnection();
            AbstractRedisAsyncCommands<byte[], byte[]> redisAsyncCommands = (AbstractRedisAsyncCommands<byte[], byte[]>) connection.getNativeConnection();

            Map<String, String> dataMap = rdmContext.getData();
            String cursor = dataMap.get("cursor");
            ScanCursor scanCursor = StringUtil.isEmpty(cursor) ? ScanCursor.INITIAL : ScanCursor.of(cursor);

            ScanArgs scanArgs = new ScanArgs().limit(10);
            RedisFuture<ScoredValueScanCursor<byte[]>> future = redisAsyncCommands.zscan(key.getBytes(), scanCursor, scanArgs);
            ScoredValueScanCursor<byte[]> scoredValueScanCursor;
            try {
                scoredValueScanCursor = future.get();
            } catch (Exception e) {
                LOGGER.error("分页获取zset异常:{}" + e.getMessage(), e);
                return dataVo;
            }

            List<ScoredValue<byte[]>> scoredValueList = scoredValueScanCursor.getValues();
          /*Map<byte[], Double> zsetData = new HashMap<>(16);
            for (ScoredValue<byte[]> scoredValue : scoredValueList) {
                double score = scoredValue.getScore();
                zsetData.put(scoredValue.getValue(), score);
            }*/
            Map<String, Double> zsetData = new HashMap<>(16);
            for (ScoredValue<byte[]> scoredValue : scoredValueList) {
                double score = scoredValue.getScore();
                zsetData.put(new String(scoredValue.getValue()), score);
            }
            String nextCursor = scoredValueScanCursor.getCursor();
            dataVo.setCursor(nextCursor);
            dataVo.setDataType(DataType.ZSET.code());
            dataVo.setTtl(RedisUtil.ttl(key));
            dataVo.setTotal(RedisUtil.zcard(key));
            dataVo.setData(zsetData);
            return dataVo;
        } finally {
            // 释放连接到连接池中
            if (connection != null) {
                connection.close();
            }
        }

    }

    private BaseDataVo zscore(String key, String value) {
        HashDataVo dataVo = new HashDataVo();
        Double score = RedisUtil.zscore(key, value);
        dataVo.setData(score);
        dataVo.setCursor(CommonConstant.STR_ZERO);
        dataVo.setDataType(DataType.ZSET.code());
        return dataVo;
    }

}
