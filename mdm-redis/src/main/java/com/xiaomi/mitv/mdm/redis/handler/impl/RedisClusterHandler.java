package com.xiaomi.mitv.mdm.redis.handler.impl;

import com.xiaomi.mitv.mdm.common.constants.CommonConstant;
import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.redis.common.annotation.Handler;
import com.xiaomi.mitv.mdm.redis.common.cache.RdmCache;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.common.model.RedisProperties;
import com.xiaomi.mitv.mdm.redis.common.serializer.RdmStringRedisSerializer;
import com.xiaomi.mitv.mdm.redis.handler.RdmHandler;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.time.Duration;

/**
 * Redis集群处理器
 */
@Handler(order = 4)
public class RedisClusterHandler implements RdmHandler {

    @Override
    public boolean boolHandle() {
        return StringUtil.equals(CommonConstant.CLUSTER, RdmContext.currentContext().getMode());
    }

    @Override
    public void handle() {
        // 当前线程本地变量
        RdmContext rdmContext = RdmContext.currentContext();
        RedisProperties redisProperties = rdmContext.getRedisProperties();
        String token = rdmContext.getToken();
        // 看是否有缓存的redisTemplate
        RedisTemplate<String, Object> redisTemplate = RdmCache.REDIS_TEMPLATE_CACHE.get(token);
        if (redisTemplate != null) {
            rdmContext.setRedisTemplate(redisTemplate);
            return;
        }
        // 集群服务配置
        // 集群redis连接工厂配置
        RedisClusterConfiguration serverConfig = new RedisClusterConfiguration(redisProperties.getNodes());
        serverConfig.setPassword(redisProperties.getPassword());
        // 用户名、密码
        serverConfig.setUsername(redisProperties.getUsername());
        serverConfig.setPassword(redisProperties.getPassword());
        // 跨集群执行命令最大重定向次数
        serverConfig.setMaxRedirects(redisProperties.getMaxRedirects());
        // 连接池信息
        GenericObjectPoolConfig<?> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxIdle(redisProperties.getPool().getMaxIdle());
        poolConfig.setMinIdle(redisProperties.getPool().getMinIdle());
        poolConfig.setMaxTotal(redisProperties.getPool().getMaxActive());
        poolConfig.setMaxWaitMillis(redisProperties.getPool().getMaxWait());

        // 连接池客户端配置建造者
        LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder lettucePoolClientConfBuilder = LettucePoolingClientConfiguration.builder()
                .commandTimeout(Duration.ofMillis(redisProperties.getTimeout()));
        // 连接池
        lettucePoolClientConfBuilder.poolConfig(poolConfig);

        // 客户端配置
        LettuceClientConfiguration clientConfig = lettucePoolClientConfBuilder.build();

        LettuceConnectionFactory lettuceConnectionFactory = new LettuceConnectionFactory(serverConfig, clientConfig);
        lettuceConnectionFactory.afterPropertiesSet();

        redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(lettuceConnectionFactory);
        // 序列化方式全部为String
        redisTemplate.setKeySerializer(RedisSerializer.string());
        redisTemplate.setValueSerializer(new RdmStringRedisSerializer());
        redisTemplate.setHashKeySerializer(RedisSerializer.string());
        redisTemplate.setHashValueSerializer(RedisSerializer.json());

        redisTemplate.afterPropertiesSet();

        rdmContext.setRedisTemplate(redisTemplate);
        // 缓存起来
        RdmCache.REDIS_TEMPLATE_CACHE.put(token, redisTemplate);
    }
}
