package com.xiaomi.mitv.mdm.redis.processor;

import com.xiaomi.mitv.mdm.redis.common.annotation.RedisDataType;
import com.xiaomi.mitv.mdm.redis.common.util.RedisUtil;
import com.xiaomi.mitv.mdm.redis.vo.BaseDataVo;
import org.springframework.data.redis.connection.DataType;

@RedisDataType("string")
public class RedisStringProcessor extends AbstractRedisDataProcessor {

    /**
     * 给定key获取数据
     *
     * @param key
     * @return
     */
    @Override
    public BaseDataVo process(String key) {
        BaseDataVo dataVo = new BaseDataVo();
        dataVo.setData(RedisUtil.get(key));
        dataVo.setTtl(RedisUtil.ttl(key));
        dataVo.setDataType(DataType.STRING.code());
        return dataVo;
    }

}
