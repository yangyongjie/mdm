package com.xiaomi.mitv.mdm.redis.processor;

import com.xiaomi.mitv.mdm.redis.common.annotation.RedisDataType;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * redis的数据处理器
 * 1、扫描包中@RedisDataType注解
 * 2、将注解中的value作为key，对应类作为value，初始化processor_map
 */
@Component
public class RedisDataProcessorHolder implements ApplicationContextAware {

    /**
     * 保存处理器的map，key为action，value为处理器Class
     */
    private static final Map<String, AbstractRedisDataProcessor> REDIS_DATA_PROCESSOR_MAP = new ConcurrentHashMap<>(4);

    public static AbstractRedisDataProcessor getRedisDataProcessorInstance(String dataType) {
        return REDIS_DATA_PROCESSOR_MAP.get(dataType);
    }

    /**
     * 扫描@ListenerAction，初始化ListenerHandlerContext，将其注册到Spring容器中
     *
     * @param applicationContext
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        // 遍历所有带有@ListenerAction注解的类
        Map<String, Object> redisDataProcessorBeans = applicationContext.getBeansWithAnnotation(RedisDataType.class);
        if (!CollectionUtils.isEmpty(redisDataProcessorBeans)) {
            for (Object listenerHandlerBean : redisDataProcessorBeans.values()) {
                String dataType = listenerHandlerBean.getClass().getAnnotation(RedisDataType.class).value();
                REDIS_DATA_PROCESSOR_MAP.put(dataType, (AbstractRedisDataProcessor) listenerHandlerBean);
            }
        }
    }
}
