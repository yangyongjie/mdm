package com.xiaomi.mitv.mdm.redis.handler.impl;

import com.xiaomi.mitv.mdm.common.constants.CommonConstant;
import com.xiaomi.mitv.mdm.common.result.ResponseEnum;
import com.xiaomi.mitv.mdm.common.util.Base64Util;
import com.xiaomi.mitv.mdm.common.util.JacksonUtil;
import com.xiaomi.mitv.mdm.common.util.Md5Util;
import com.xiaomi.mitv.mdm.common.util.RequestUtil;
import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.redis.common.annotation.Handler;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.common.model.RedisProperties;
import com.xiaomi.mitv.mdm.redis.common.queue.NettyDelayQueue;
import com.xiaomi.mitv.mdm.redis.handler.RdmHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;

/**
 * 参数处理器
 */
@Handler(order = 2)
public class ParamParseHandler implements RdmHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParamParseHandler.class);

    @Override
    public boolean boolHandle() {
        return true;
    }

    @Override
    public void handle() {
        RdmContext rdmContext = RdmContext.currentContext();
        try {
            // 将data从base64转为json明文
            String originData = RequestUtil.getParam("data");
            String data = Base64Util.decode(originData);
            // 解析其中的连接信息
            Map<String, String> dataMap = JacksonUtil.ofMap(data, String.class, String.class);
            if (dataMap == null) {
                rdmContext.getHandleResult().fail(ResponseEnum.PARAM_ERROR);
                return;
            }
            rdmContext.setData(dataMap);
            // 单机还是集群
            String mode = dataMap.get("mode");
            String host = dataMap.get("host");
            String password = dataMap.get("password");
            String username = dataMap.get("username");
            String database = dataMap.get("database");

            rdmContext.setMode(mode);
            RedisProperties redisProperties = new RedisProperties();
            if (StringUtil.equals(CommonConstant.STANDALONE, mode)) {
                // 解析域名端口
                String[] hostPort = host.split(":");
                redisProperties.setHost(hostPort[0]);
                redisProperties.setPort(Integer.parseInt(hostPort[1]));
                // 如果数据库不为空
                if (StringUtil.isNotEmpty(database)) {
                    redisProperties.setDatabase(Integer.parseInt(database));
                }
            } else if (StringUtil.equals(CommonConstant.CLUSTER, mode)) {
                String[] hosts = host.split(",");
                redisProperties.setNodes(Arrays.asList(hosts));
            } else {
                rdmContext.getHandleResult().fail(ResponseEnum.PARAM_ERROR);
                return;
            }
            redisProperties.setUsername(username);
            redisProperties.setPassword(password);
            rdmContext.setRedisProperties(redisProperties);
            String token = Md5Util.getMd5(JacksonUtil.toString(redisProperties));
            rdmContext.setToken(token);
            // 放入延时队列中，半小时后清理
            NettyDelayQueue.delayClearToken(token);
        } catch (Exception e) {
            LOGGER.error("参数处理失败：" + e.getMessage(), e);
            rdmContext.getHandleResult().fail(ResponseEnum.PARAM_ERROR);
        }
    }

}
