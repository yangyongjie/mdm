package com.xiaomi.mitv.mdm.redis.processor;

import com.xiaomi.mitv.mdm.redis.common.annotation.RedisDataType;
import com.xiaomi.mitv.mdm.redis.common.util.RedisUtil;
import com.xiaomi.mitv.mdm.redis.vo.BaseDataVo;
import com.xiaomi.mitv.mdm.redis.vo.StreamDataVo;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RedisDataType("stream")
public class RedisStreamProcessor extends AbstractRedisDataProcessor {

    @Override
    public BaseDataVo process(String key) {
        StreamDataVo dataVo = new StreamDataVo();
        dataVo.setDataType(DataType.STREAM.code());
        dataVo.setTtl(-1L);
        dataVo.setTotal(RedisUtil.xlen(key));
        List<MapRecord<String, Object, Object>> mapRecordList = RedisUtil.xrange(key);
        if (CollectionUtils.isEmpty(mapRecordList)) {
            return dataVo;
        }
        Map<Object, Object> data = new HashMap<>(4);
        dataVo.setData(data);
        for (MapRecord<String, Object, Object> mapRecord : mapRecordList) {
            // RecordId recordId = mapRecord.getId();
            Map<Object, Object> value = mapRecord.getValue();
            data.putAll(value);
        }
        return dataVo;
    }

}
