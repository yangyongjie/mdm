package com.xiaomi.mitv.mdm.redis.controller;

import com.xiaomi.mitv.mdm.common.result.HandleResult;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取info信息
 */
@RestController
@RequestMapping("rdm/server")
public class RedisServerController {

    @GetMapping("info")
    public HandleResult info() {
        RedisTemplate<String, Object> redisTemplate = RdmContext.currentContext().getRedisTemplate();
        List<Map<String, Object>> result = redisTemplate.execute((RedisCallback<List<Map<String, Object>>>) connection -> {
            List<Map<String, Object>> dbInfo = new ArrayList<>();
//            Properties properties = connection.serverCommands().info();
            Object infoObj = connection.execute("info", null);
            String infoData = (String) infoObj;
            String[] infoArr = infoData.split("\\r?\\n");
            int count = infoArr.length;
            String key = "";
            Map<String, Object> data = null;
            Map<String, Object> dbMap = new HashMap<>();
            for (int i = 0; i < count; i++) {
                if ("".equals(infoArr[i])) {
                    continue;
                }
                if (!infoArr[i].contains("#")) {
                    if (data == null) {
                        data = new HashMap<>();
                    }
                    data.put(infoArr[i].split(":")[0], infoArr[i].split(":")[1]);
                } else {
                    if (i != 0) {
                        dbMap.put(key, data);
                        data = null;
                    }
                    key = infoArr[i].replace('#', ' ').trim();
                }
                if (i == count - 1) {
                    dbMap.put(key, data);
                    data = null;
                }
            }
            dbInfo.add(dbMap);
            return dbInfo;
        });
        return new HandleResult(result);
    }
}