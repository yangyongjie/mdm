package com.xiaomi.mitv.mdm.redis.handler.impl;

import com.xiaomi.mitv.mdm.common.constants.CommonConstant;
import com.xiaomi.mitv.mdm.common.result.ResponseEnum;
import com.xiaomi.mitv.mdm.common.util.RequestUtil;
import com.xiaomi.mitv.mdm.common.util.RsaUtil;
import com.xiaomi.mitv.mdm.redis.common.annotation.Handler;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.handler.RdmHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 签名处理器
 */
@Handler(order = 1)
public class SignVerifyHandler implements RdmHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignVerifyHandler.class);

    @Override
    public boolean boolHandle() {
        return true;
    }

    @Override
    public void handle() {
        try {
            // 获取签名
            String sign = RequestUtil.getParam("sign");
            // 获取参数
            String data = RequestUtil.getParam("data");
            // 校验
            if (!RsaUtil.rsaCheck(data, sign, CommonConstant.PUBLIC_KEY)) {
                RdmContext.currentContext().getHandleResult().fail(ResponseEnum.SIGN_ERROR);
            }
        } catch (Exception e) {
            LOGGER.error("验签失败：" + e.getMessage(), e);
            RdmContext.currentContext().getHandleResult().fail(ResponseEnum.SIGN_ERROR);
        }
    }
}
