package com.xiaomi.mitv.mdm.redis.common.model;

import java.util.List;

public class RedisProperties {
    private String host;
    private int port;
    private String username;
    private String password;
    private int database = 0;
    private Long timeout = 5000L;

    /**
     * 逗号分隔的"host:port"列表，至少要有一个
     */
    private List<String> nodes;

    /**
     * 在集群中执行命令时要遵循的最大重定向数
     */
    private Integer maxRedirects = 3;

    private final Pool pool = new Pool();

    /**
     * 连接池配置属性
     */
    public static class Pool {
        private int maxIdle = 8; //  连接池内空闲连接的最大数量，使用负值表示没有限制
        private int minIdle = 1; // 连接池内维护的最小空闲连接数，值为正时才有效
        private int maxActive = 8; // 给定时间连接池内最大连接数，使用负值表示没有限制
        private long maxWait = -1; // 当池耗尽时，在抛出异常之前，连接分配应阻塞的最长时间。使用负值可无限期阻止

        public int getMaxIdle() {
            return maxIdle;
        }

        public void setMaxIdle(int maxIdle) {
            this.maxIdle = maxIdle;
        }

        public int getMinIdle() {
            return minIdle;
        }

        public void setMinIdle(int minIdle) {
            this.minIdle = minIdle;
        }

        public int getMaxActive() {
            return maxActive;
        }

        public void setMaxActive(int maxActive) {
            this.maxActive = maxActive;
        }

        public long getMaxWait() {
            return maxWait;
        }

        public void setMaxWait(long maxWait) {
            this.maxWait = maxWait;
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getDatabase() {
        return database;
    }

    public void setDatabase(int database) {
        this.database = database;
    }

    public Long getTimeout() {
        return timeout;
    }

    public void setTimeout(Long timeout) {
        this.timeout = timeout;
    }

    public List<String> getNodes() {
        return nodes;
    }

    public void setNodes(List<String> nodes) {
        this.nodes = nodes;
    }

    public Integer getMaxRedirects() {
        return maxRedirects;
    }

    public void setMaxRedirects(Integer maxRedirects) {
        this.maxRedirects = maxRedirects;
    }

    public Pool getPool() {
        return pool;
    }
}
