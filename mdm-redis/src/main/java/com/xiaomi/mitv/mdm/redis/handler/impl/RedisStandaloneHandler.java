package com.xiaomi.mitv.mdm.redis.handler.impl;

import com.xiaomi.mitv.mdm.common.constants.CommonConstant;
import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.redis.common.annotation.Handler;
import com.xiaomi.mitv.mdm.redis.common.cache.RdmCache;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.common.model.RedisProperties;
import com.xiaomi.mitv.mdm.redis.common.serializer.RdmStringRedisSerializer;
import com.xiaomi.mitv.mdm.redis.handler.RdmHandler;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.time.Duration;

/**
 * Redis单机处理器
 */
@Handler(order = 3)
public class RedisStandaloneHandler implements RdmHandler {

    @Override
    public boolean boolHandle() {
        return StringUtil.equals(CommonConstant.STANDALONE,RdmContext.currentContext().getMode());
    }

    @Override
    public void handle() {
        RdmContext rdmContext = RdmContext.currentContext();
        RedisProperties redisProperties = rdmContext.getRedisProperties();
        String token = rdmContext.getToken();
        // 看是否有缓存的redisTemplate
        RedisTemplate<String, Object> redisTemplate = RdmCache.REDIS_TEMPLATE_CACHE.get(token);
        if (redisTemplate != null) {
            rdmContext.setRedisTemplate(redisTemplate);
            return;
        }
        // 单机服务配置
        RedisStandaloneConfiguration serverConfig = new RedisStandaloneConfiguration();
        // 主机、用户名、密码、端口、数据库
        serverConfig.setHostName(redisProperties.getHost());
        serverConfig.setPassword(redisProperties.getPassword());
        serverConfig.setPort(redisProperties.getPort());
        serverConfig.setDatabase(redisProperties.getDatabase());
        serverConfig.setUsername(redisProperties.getUsername());
        // 连接池信息
        GenericObjectPoolConfig<?> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxIdle(redisProperties.getPool().getMaxIdle());
        poolConfig.setMinIdle(redisProperties.getPool().getMinIdle());
        poolConfig.setMaxTotal(redisProperties.getPool().getMaxActive());
        poolConfig.setMaxWaitMillis(redisProperties.getPool().getMaxWait());

        // 连接池客户端配置建造者
        LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder lettucePoolClientConfBuilder = LettucePoolingClientConfiguration.builder()
                .commandTimeout(Duration.ofMillis(redisProperties.getTimeout()));
        // 连接池
        lettucePoolClientConfBuilder.poolConfig(poolConfig);

        // 客户端配置
        LettuceClientConfiguration clientConfig = lettucePoolClientConfBuilder.build();

        LettuceConnectionFactory lettuceConnectionFactory = new LettuceConnectionFactory(serverConfig, clientConfig);
        lettuceConnectionFactory.afterPropertiesSet();

        redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(lettuceConnectionFactory);
        // 序列化方式全部为String
        redisTemplate.setKeySerializer(RedisSerializer.string());
        redisTemplate.setValueSerializer(new RdmStringRedisSerializer());
        redisTemplate.setHashKeySerializer(RedisSerializer.string());
        redisTemplate.setHashValueSerializer(RedisSerializer.json());

        redisTemplate.afterPropertiesSet();

        rdmContext.setRedisTemplate(redisTemplate);
        // 缓存起来
        RdmCache.REDIS_TEMPLATE_CACHE.put(token, redisTemplate);
    }
}
