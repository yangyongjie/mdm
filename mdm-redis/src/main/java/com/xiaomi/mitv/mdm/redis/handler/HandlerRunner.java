package com.xiaomi.mitv.mdm.redis.handler;

import com.xiaomi.mitv.mdm.common.result.HandleResult;
import com.xiaomi.mitv.mdm.redis.common.annotation.Handler;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * 处理器注册器及执行器
 */
@Component
public class HandlerRunner implements ApplicationContextAware {

    private static final List<RdmHandler> HANDLERS = new ArrayList<>();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        // 加载并注册处理器
        Map<String, RdmHandler> handlerMap = applicationContext.getBeansOfType(RdmHandler.class);
        for (Map.Entry<String, RdmHandler> entry : handlerMap.entrySet()) {
            // 注册处理器
            HANDLERS.add(entry.getValue());
        }
        // 排序
        HANDLERS.sort(Comparator.comparingInt(o -> o.getClass().getAnnotation(Handler.class).order()));
    }

    public static HandleResult run() {
        // 从上下文中获取处理器的执行结果（引用）
        HandleResult handleResult = RdmContext.currentContext().getHandleResult();
        // 遍历处理器,如果需要执行并执行
        for (RdmHandler handler : HANDLERS) {
            if (handler.boolHandle()) {
                handler.handle();
            }
            // 如果执行失败，终止执行
            if (!handleResult.isSuccess()) {
                break;
            }
        }
        // 返回执行结果
        return handleResult;
    }
}
