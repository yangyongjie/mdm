package com.xiaomi.mitv.mdm.redis.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * stream类型数据返回
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StreamDataVo extends BaseDataVo {
    /**
     * 总条数
     */
    private Long total;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
