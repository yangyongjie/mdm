package com.xiaomi.mitv.mdm.redis.common.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 处理器注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Component
public @interface Handler {
    /**
     * 执行顺序
     *
     * @return
     */
    int order();

    /**
     * 处理器类型
     *
     * @return
     */
    String handlerType() default "";
}
