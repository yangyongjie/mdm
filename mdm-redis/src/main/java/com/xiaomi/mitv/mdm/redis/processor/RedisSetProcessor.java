package com.xiaomi.mitv.mdm.redis.processor;

import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.redis.common.annotation.RedisDataType;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.common.util.RedisUtil;
import com.xiaomi.mitv.mdm.redis.vo.BaseDataVo;
import com.xiaomi.mitv.mdm.redis.vo.HashDataVo;
import io.lettuce.core.AbstractRedisAsyncCommands;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.ScanCursor;
import io.lettuce.core.ValueScanCursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnection;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RedisDataType("set")
public class RedisSetProcessor extends AbstractRedisDataProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisSetProcessor.class);

    @Override
    public BaseDataVo process(String key) {
        HashDataVo dataVo = new HashDataVo();

        RdmContext rdmContext = RdmContext.currentContext();
        RedisTemplate<String, Object> redisTemplate = rdmContext.getRedisTemplate();
        LettuceConnection connection = null;
        try {
            RedisConnectionFactory redisConnectionFactory = redisTemplate.getConnectionFactory();
            connection = (LettuceConnection) redisConnectionFactory.getConnection();
            AbstractRedisAsyncCommands<byte[], byte[]> redisAsyncCommands = (AbstractRedisAsyncCommands<byte[], byte[]>) connection.getNativeConnection();

            Map<String, String> dataMap = rdmContext.getData();
            String cursor = dataMap.get("cursor");
            ScanCursor scanCursor = StringUtil.isEmpty(cursor) ? ScanCursor.INITIAL : ScanCursor.of(cursor);

            ScanArgs scanArgs = new ScanArgs().limit(10);
            RedisFuture<ValueScanCursor<byte[]>> future = redisAsyncCommands.sscan(key.getBytes(), scanCursor, scanArgs);
            ValueScanCursor<byte[]> valueScanCursor;
            try {
                valueScanCursor = future.get();
            } catch (Exception e) {
                LOGGER.error("分页获取set异常:{}" + e.getMessage(), e);
                return dataVo;
            }
            List<byte[]> values = valueScanCursor.getValues();
            // dataVo.setData(values);
            List<String> data = new ArrayList<>();
            for (byte[] value : values) {
                data.add(new String(value));
            }

            String nextCursor = valueScanCursor.getCursor();
            dataVo.setCursor(nextCursor);
            dataVo.setDataType(DataType.SET.code());
            dataVo.setTtl(RedisUtil.ttl(key));
            dataVo.setTotal(RedisUtil.scard(key));
            dataVo.setData(data);
            return dataVo;
        } finally {
            // 释放连接到连接池中
            if (connection != null) {
                connection.close();
            }
        }
    }

}
