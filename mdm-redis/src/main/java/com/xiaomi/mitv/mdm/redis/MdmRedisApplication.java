package com.xiaomi.mitv.mdm.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MdmRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MdmRedisApplication.class, args);
    }

}
