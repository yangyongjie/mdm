package com.xiaomi.mitv.mdm.redis.common.queue;

import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.redis.common.cache.RdmCache;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;
import io.netty.util.concurrent.DefaultThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 封装Netty的时间轮来实现延时队列
 */
public class NettyDelayQueue {

    private static final Logger LOGGER = LoggerFactory.getLogger(NettyDelayQueue.class);

    /**
     * 执行延时任务的线程池
     * 假设服务器核心数core为6 核心线程为core+1，最大线程数为2*core，等待队列可设置较大一些
     */
    private static final ThreadPoolExecutor DELAY_TASK_POOLER = new ThreadPoolExecutor(
            7,
            12,
            60,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(10000),
            new DefaultThreadFactory("netty-delay"),
            new RejectedExecutionHandler() {
                @Override
                public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                    // 打印日志
                    LOGGER.error("Task：{},rejected from：{}", r.toString(), executor.toString());
                    // 直接执行被拒绝的任务，JVM另起线程执行
                    r.run();
                }
            }
    );

    /**
     * 构建时间轮
     */
    private static final HashedWheelTimer HASHED_WHEEL_TIMER = new HashedWheelTimer(
            new DefaultThreadFactory("wheel-timer"),
            100,
            TimeUnit.MILLISECONDS,
            512,
            true,
            -1L
    );

    /**
     * 30分钟后清除token指定的Redis连接
     *
     * @param token
     */
    public static void delayClearToken(String token) {
        HASHED_WHEEL_TIMER.newTimeout(new TimerTask() {
            @Override
            public void run(Timeout timeout) throws Exception {
                if (!StringUtil.isEmpty(token)) {
                    LOGGER.info("连接:{},被断开", token);
                    RdmCache.REDIS_TEMPLATE_CACHE.remove(token);
                }
            }
        }, 30 * 60L, TimeUnit.SECONDS);
    }
}
