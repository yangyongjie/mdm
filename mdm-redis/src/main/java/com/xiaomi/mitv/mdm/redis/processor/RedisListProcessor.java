package com.xiaomi.mitv.mdm.redis.processor;

import com.xiaomi.mitv.mdm.common.constants.CommonConstant;
import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.redis.common.annotation.RedisDataType;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.common.util.RedisUtil;
import com.xiaomi.mitv.mdm.redis.vo.BaseDataVo;
import com.xiaomi.mitv.mdm.redis.vo.HashDataVo;
import org.springframework.data.redis.connection.DataType;

import java.util.List;
import java.util.Map;

@RedisDataType("list")
public class RedisListProcessor extends AbstractRedisDataProcessor {

    @Override
    public BaseDataVo process(String key) {
        RdmContext context = RdmContext.currentContext();
        Map<String, String> data = context.getData();
        // 判断index字段是否为空，为空直接查询单个
        String index = data.get("index");
        if (StringUtil.isNotEmpty(index)) {
            return lindex(key, Long.parseLong(index));
        }

        HashDataVo dataVo = new HashDataVo();
        // 总数
        Long total = RedisUtil.llen(key);
        // 每页固定20条
        int pageSize = 20;
        // 获取总页数
        long pageCount = (total % pageSize == 0) ? total / pageSize : total / pageSize + 1;

        // 获取请求参数总的当前页码参数
        int pageIndex = StringUtil.isEmpty(data.get("cursor")) ? 1 : Integer.parseInt(data.get("cursor"));

        // 开始偏移量
        long start = (long) (pageIndex - 1) * pageSize;
        long end = start + pageSize - 1;
        List<Object> value = RedisUtil.lrange(key, start, end);

        dataVo.setData(value);
        dataVo.setTotal(total);
        dataVo.setTtl(RedisUtil.ttl(key));
        dataVo.setCursor(pageIndex >= pageCount ? CommonConstant.STR_ZERO : String.valueOf(pageIndex + 1));
        dataVo.setDataType(DataType.LIST.code());
        return dataVo;
    }

    private BaseDataVo lindex(String key, long index) {
        HashDataVo dataVo = new HashDataVo();
        // 总数
        Long total = RedisUtil.llen(key);
        dataVo.setData(RedisUtil.lindex(key, index));
        dataVo.setTotal(total);
        dataVo.setTtl(RedisUtil.ttl(key));
        dataVo.setCursor(CommonConstant.STR_ZERO);
        dataVo.setDataType(DataType.LIST.code());
        return dataVo;
    }

}
