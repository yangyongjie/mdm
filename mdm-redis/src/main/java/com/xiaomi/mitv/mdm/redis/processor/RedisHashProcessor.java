package com.xiaomi.mitv.mdm.redis.processor;

import com.xiaomi.mitv.mdm.common.constants.CommonConstant;
import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.common.util.ZlibUtil;
import com.xiaomi.mitv.mdm.redis.common.annotation.RedisDataType;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.common.util.RedisUtil;
import com.xiaomi.mitv.mdm.redis.vo.BaseDataVo;
import com.xiaomi.mitv.mdm.redis.vo.HashDataVo;
import io.lettuce.core.AbstractRedisAsyncCommands;
import io.lettuce.core.MapScanCursor;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.ScanCursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnection;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;

@RedisDataType("hash")
public class RedisHashProcessor extends AbstractRedisDataProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisHashProcessor.class);

    @Override
    public BaseDataVo process(String key) {
        RdmContext rdmContext = RdmContext.currentContext();
        // 判断field参数为不为空
        Map<String, String> data = rdmContext.getData();
        String field = data.get("field");
        if (StringUtil.isNotEmpty(field)) {
            return hget(key, field);
        }
        HashDataVo dataVo = new HashDataVo();
        RedisTemplate<String, Object> redisTemplate = rdmContext.getRedisTemplate();
        LettuceConnection connection = null;
        try {
            RedisConnectionFactory redisConnectionFactory = redisTemplate.getConnectionFactory();
            connection = (LettuceConnection) redisConnectionFactory.getConnection();
            AbstractRedisAsyncCommands<byte[], byte[]> redisAsyncCommands = (AbstractRedisAsyncCommands<byte[], byte[]>) connection.getNativeConnection();

            Map<String, String> dataMap = rdmContext.getData();
            String cursor = dataMap.get("cursor");
            ScanCursor scanCursor = StringUtil.isEmpty(cursor) ? ScanCursor.INITIAL : ScanCursor.of(cursor);

            ScanArgs scanArgs = new ScanArgs().limit(30);
            RedisFuture<MapScanCursor<byte[], byte[]>> future = redisAsyncCommands.hscan(key.getBytes(), scanCursor, scanArgs);
            MapScanCursor<byte[], byte[]> mapScanCursor;
            try {
                mapScanCursor = future.get();
            } catch (Exception e) {
                LOGGER.error("分页获取Hash异常:{}" + e.getMessage(), e);
                return dataVo;
            }

            Map<Object, Object> resultMap = new HashMap<>(16);
            for (Map.Entry<byte[], byte[]> entry : mapScanCursor.getMap().entrySet()) {
                try {
                    resultMap.put(redisTemplate.getHashKeySerializer().deserialize(entry.getKey()),
                            redisTemplate.getHashValueSerializer().deserialize(ZlibUtil.decompress(entry.getValue())));
                } catch (Exception e) {
                    resultMap.put(redisTemplate.getHashKeySerializer().deserialize(entry.getKey()), new String(ZlibUtil.decompress(entry.getValue())));
                }
            }
            String nextCursor = mapScanCursor.getCursor();
            dataVo.setCursor(nextCursor);
            dataVo.setData(resultMap);
            dataVo.setDataType(DataType.HASH.code());
            dataVo.setTtl(RedisUtil.ttl(key));
            dataVo.setTotal(RedisUtil.hlen(key));
            return dataVo;
        } finally {
            // 释放连接到连接池中
            if (connection != null) {
                connection.close();
            }
        }
    }

    private BaseDataVo hget(String key, String field) {
        HashDataVo dataVo = new HashDataVo();
        dataVo.setData(RedisUtil.hget(key, field));
        dataVo.setDataType(DataType.HASH.code());
        dataVo.setTtl(RedisUtil.ttl(key));
        dataVo.setCursor(CommonConstant.STR_ZERO);
        return dataVo;
    }

}
