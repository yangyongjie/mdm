package com.xiaomi.mitv.mdm.redis.vo;

public class BaseDataVo {
    /**
     * 数据类型
     * none (key不存在)
     * string (字符串)
     * list (列表)
     * set (集合)
     * zset (有序集)
     * hash (哈希表)
     */
    private String dataType;
    /**
     * 生存时间，单位秒，没设置则为-1
     */
    private Long ttl;
    /**
     * 缓存数据
     */
    private Object data;

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Long getTtl() {
        return ttl;
    }

    public void setTtl(Long ttl) {
        this.ttl = ttl;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
