package com.xiaomi.mitv.mdm.redis.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * hash或set或zset类型数据返回
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HashDataVo extends BaseDataVo {

    /**
     * 总条数
     */
    private Long total;

    /**
     * 下一页游标
     */
    private String cursor;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }
}
