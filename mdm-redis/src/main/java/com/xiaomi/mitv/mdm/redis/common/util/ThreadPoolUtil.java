package com.xiaomi.mitv.mdm.redis.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.util.concurrent.*;

/**
 * 基础公共线程池
 *
 */
public class ThreadPoolUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadPoolUtil.class);

    private static final ThreadPoolExecutor THREAD_POLL_EXECUTOR = new ThreadPoolExecutor(5, 50, 5000, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(10000), new RejectedExecutionHandler() {
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            LOGGER.error("ThreadPool too many tasks wait for handle resulting overflow,detail task is:" + r.toString());
        }
    });

    public static void submit(Runnable task) {
        THREAD_POLL_EXECUTOR.submit(task);
    }

    public static <T> Future<T> submit(Callable<T> task) {

        return THREAD_POLL_EXECUTOR.submit(task);
    }

    @PreDestroy
    public void shutDown() {
        LOGGER.info("ThreadPool shutdown .");
        THREAD_POLL_EXECUTOR.shutdown();
    }

}
