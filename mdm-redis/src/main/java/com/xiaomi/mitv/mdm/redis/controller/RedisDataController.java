package com.xiaomi.mitv.mdm.redis.controller;

import com.xiaomi.mitv.mdm.common.result.HandleResult;
import com.xiaomi.mitv.mdm.common.result.ResponseEnum;
import com.xiaomi.mitv.mdm.common.util.StringUtil;
import com.xiaomi.mitv.mdm.redis.common.context.RdmContext;
import com.xiaomi.mitv.mdm.redis.common.util.RedisUtil;
import com.xiaomi.mitv.mdm.redis.processor.RedisDataProcessorHolder;
import com.xiaomi.mitv.mdm.redis.vo.BaseDataVo;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 数据查询
 */
@RestController
@RequestMapping("rdm/data")
public class RedisDataController {

    /**
     * 左侧点击key数据获取
     *
     * @return
     */
    @RequestMapping("obtain")
    public HandleResult obtain() {
        RdmContext rdmContext = RdmContext.currentContext();
        RedisTemplate<String, Object> redisTemplate = rdmContext.getRedisTemplate();
        // 请求参数
        Map<String, String> dataMap = rdmContext.getData();
        // 获取key
        String key = dataMap.get("key");
        // 获取key的数据类型
        DataType dataType = redisTemplate.execute((RedisCallback<DataType>) connection -> connection.keyCommands().type(key.getBytes(StandardCharsets.UTF_8)));
        // 根据类型找到对应的服务处理
        String code = dataType.code();
        if (StringUtil.equals(DataType.NONE.code(), code)) {
            return new HandleResult(ResponseEnum.KEY_NOT_EXISTS);
        }
        BaseDataVo dataVo = RedisDataProcessorHolder.getRedisDataProcessorInstance(code).process(key);
        return new HandleResult(dataVo);
    }

    /**
     * 修改生存时间
     *
     * @return
     */
    @RequestMapping("expire")
    public HandleResult expire() {
        RdmContext rdmContext = RdmContext.currentContext();
        // 请求参数
        Map<String, String> dataMap = rdmContext.getData();
        // 获取key
        String key = dataMap.get("key");
        // 获取生存时间
        long ttl = Long.parseLong(dataMap.get("ttl"));
        boolean isSucc = RedisUtil.expire(key, ttl);
        return isSucc ? new HandleResult() : new HandleResult(ResponseEnum.UNKNOWN_ERROR);
    }

    /**
     * 新增key
     */
    @RequestMapping("del")
    public HandleResult del() {
        RdmContext rdmContext = RdmContext.currentContext();
        // 请求参数
        Map<String, String> dataMap = rdmContext.getData();
        // 获取key
        String key = dataMap.get("key");
        boolean isSucc = RedisUtil.del(key);
        return isSucc ? new HandleResult() : new HandleResult(ResponseEnum.UNKNOWN_ERROR);
    }

}
