package com.xiaomi.mitv.mdm.common.util;

import org.slf4j.MDC;

/**
 * 日志MDC工具类
 */
public class MDCUtil {

    private MDCUtil() {
    }

    private static final String TRACE_ID = "traceId";

    /**
     * 初始化日志格式
     */
    public static void init() {
        MDC.put(TRACE_ID, StringUtil.getUuid());
    }

    /**
     * 清除MDC
     */
    public static void remove() {
        MDC.remove(TRACE_ID);
    }
}
