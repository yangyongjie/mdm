package com.xiaomi.mitv.mdm.common.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

public class HandleResult {

    private Integer code = 0;

    private String message = "success";

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Object data;

    public HandleResult() {
    }

    public HandleResult(ResponseEnum response) {
        this.code = response.getCode();
        this.message = response.getMessage();
    }

    public HandleResult(Object data) {
        this.data = data;
    }

    public HandleResult fail(ResponseEnum response) {
        this.code = response.getCode();
        this.message = response.getMessage();
        return this;
    }

    @JsonIgnore
    public boolean isSuccess() {
        return ResponseEnum.SUCCESS.getCode().equals(code);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
