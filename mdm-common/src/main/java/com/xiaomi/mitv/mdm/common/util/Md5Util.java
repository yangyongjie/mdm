package com.xiaomi.mitv.mdm.common.util;

import java.security.MessageDigest;

public class Md5Util {
    private Md5Util() {
    }

    public static String getMd5(String message) {
        return message != null && message.length() != 0 ? getMd5(message.getBytes()) : "";
    }

    public static String getMd5(byte[] bytes) {
        if (bytes == null) {
            return "";
        } else {
            String digest = "";
            try {
                MessageDigest algorithm = MessageDigest.getInstance("MD5");
                algorithm.reset();
                algorithm.update(bytes);
                digest = toHexString(algorithm.digest());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return digest;
        }
    }

    private static String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        int length = bytes.length;

        for (int i = 0; i < length; ++i) {
            byte b = bytes[i];
            String str;
            for (str = Integer.toHexString(255 & b); str.length() < 2; str = "0" + str) {
            }
            hexString.append(str);
        }
        return hexString.toString();
    }

}
