package com.xiaomi.mitv.mdm.common.util;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

/**
 * RSA工具类，默认密钥长度为1024位
 * 当用2048位密钥时：签名算法为SHA256WithRSA，签名类型为RSA2，加密算法也为RSA，最大加密明文长度为245，最大解密密文大小为256
 */
public class RsaUtil {
    private RsaUtil() {
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(RsaUtil.class);

    /**
     * 签名算法
     */
    private static final String SIGN_ALGORITHMS = "SHA1WithRSA";

    /**
     * 加密算法
     */
    private static final String RSA_ALGORITHM = "RSA";


    /**
     * RSA最大加密明文大小
     * 密钥长度为1024位的话，最大加密明文大小为117
     * 密钥长度为2048位的话，最大加密明文大小为245
     */
    private static final int MAX_ENCRYPT_BLOCK = 117;

    /**
     * RSA最大解密密文大小
     * 密钥长度为1024位的话，最大解密密文大小为128
     * 密钥长度为2048位的话，最大解密密文大小为256
     */
    private static final int MAX_DECRYPT_BLOCK = 128;

    /**
     * 公钥加密
     *
     * @param plainText 待加密内容
     * @param publicKey 公钥
     * @return 密文内容
     */
    public static String rsaEncrypt(String plainText, String publicKey) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
            PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(publicKey.getBytes())));
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            byte[] data = plainText.getBytes(StandardCharsets.UTF_8);
            int inputLen = data.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段加密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = Base64.encodeBase64(out.toByteArray());
            out.close();

            return new String(encryptedData, StandardCharsets.UTF_8);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 私钥解密
     *
     * @param cipherText 待解密内容
     * @param privateKey 私钥
     * @return 明文内容
     */
    public static String rsaDecrypt(String cipherText, String privateKey) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
            PrivateKey priKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey.getBytes())));
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, priKey);
            byte[] encryptedData = Base64.decodeBase64(cipherText.getBytes(StandardCharsets.UTF_8));
            int inputLen = encryptedData.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段解密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                    cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            out.close();
            return new String(decryptedData, StandardCharsets.UTF_8);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * sha1WithRsa 加签
     *
     * @param content
     * @param privateKey
     * @return
     */
    public static String rsaSign(String content, String privateKey) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
            PrivateKey priKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey.getBytes())));
            Signature signature = Signature.getInstance(SIGN_ALGORITHMS);
            signature.initSign(priKey);
            signature.update(content.getBytes(StandardCharsets.UTF_8));
            byte[] signed = signature.sign();
            return new String(Base64.encodeBase64(signed));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public static boolean rsaCheck(String content, String sign, String publicKey) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
            PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(publicKey.getBytes())));
            Signature signature = Signature.getInstance(SIGN_ALGORITHMS);
            signature.initVerify(pubKey);
            signature.update(content.getBytes(StandardCharsets.UTF_8));
            return signature.verify(Base64.decodeBase64(sign.getBytes()));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }

    /**
     * 生成1024位的公私钥对
     *
     * @return
     */
    public static Map<String, String> genRSAKeyPairs() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(RSA_ALGORITHM);
        // keySize
        keyPairGenerator.initialize(1024);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
        String priKey = Base64.encodeBase64String(privateKey.getEncoded());
        String pubKey = Base64.encodeBase64String(publicKey.getEncoded());
        Map<String, String> keyMap = new HashMap<>(4);
        keyMap.put("publicKey", pubKey);
        keyMap.put("privateKey", priKey);
        return keyMap;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        Map<String, String> keyMap = genRSAKeyPairs();
        String publicKey = keyMap.get("publicKey");
        String privateKey = keyMap.get("privateKey");
        // 签名验签
        String content = "eyJuYW1lIjoic2Rmc2RmIiwidHlwZSI6InNpbmdsZSIsImhvc3QiOiIxMjcuMC4wLjE6NjM3OSIsInBhc3N3b3JkIjoiIn0%253D";
        String sign = rsaSign(content, privateKey);
        boolean pass = rsaCheck(content, sign, publicKey);
        // 加密解密
        String cipherText = rsaEncrypt(content, publicKey);
        String plainText = rsaDecrypt(cipherText, privateKey);
        System.out.println(plainText);
    }
}

