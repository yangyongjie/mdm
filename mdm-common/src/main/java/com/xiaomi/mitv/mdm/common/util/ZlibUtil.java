package com.xiaomi.mitv.mdm.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * zlib压缩解压缩工具类
 */
public class ZlibUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZlibUtil.class);

    private ZlibUtil() {
    }

    public static byte[] compress(byte[] data) {
        byte[] output;
        Deflater deflater = new Deflater();
        deflater.reset();
        deflater.setInput(data);
        deflater.finish();
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length)) {
            byte[] buf = new byte[1024];
            while (!deflater.finished()) {
                int i = deflater.deflate(buf);
                bos.write(buf, 0, i);
            }
            output = bos.toByteArray();
        } catch (Exception e) {
            output = data;
            LOGGER.info(e.getMessage(), e);
        }
        deflater.end();
        return output;
    }

    public static byte[] decompress(byte[] data) {
        byte[] output;
        Inflater inflater = new Inflater();
        inflater.reset();
        inflater.setInput(data);
        try (ByteArrayOutputStream o = new ByteArrayOutputStream(data.length)) {
            byte[] buf = new byte[1024];
            while (!inflater.finished()) {
                int i = inflater.inflate(buf);
                o.write(buf, 0, i);
            }
            output = o.toByteArray();
        } catch (Exception e) {
            output = data;
            LOGGER.info(e.getMessage(), e);
        }
        inflater.end();
        return output;
    }

}
