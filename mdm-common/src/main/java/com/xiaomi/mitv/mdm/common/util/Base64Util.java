package com.xiaomi.mitv.mdm.common.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * base64编解码
 */
public class Base64Util {

    private Base64Util() {
    }

    public static String encode(String content) {
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(content.getBytes(StandardCharsets.UTF_8));
    }

    public static String decode(String content) {
        Base64.Decoder encoder = Base64.getDecoder();
        return new String(encoder.decode(content.getBytes(StandardCharsets.UTF_8)));
    }
}
