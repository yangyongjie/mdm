package com.xiaomi.mitv.mdm.common.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * 请求工具，webflux禁用
 *
 * @author liangjinxin
 */
public class RequestUtil {

    private RequestUtil() {
    }

    /**
     * 获取当前线程的请求对象
     *
     * @return HttpServletRequest
     */
    public static HttpServletRequest get() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    public static String method() {
        return get().getMethod();
    }

    public static String uri() {
        return get().getRequestURI();
    }

    public static String getParam(String key) {
        return get().getParameter(key);
    }

    public static <T> T getParam(String key, Class<T> tClass) {
        return JacksonUtil.toObject(getParam(key), tClass);
    }

}
