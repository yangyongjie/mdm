package com.xiaomi.mitv.mdm.common.constants;

/**
 * 公共常量类
 */
public class CommonConstant {
    private CommonConstant() {
    }

    public static final String STR_ZERO = "0";
    public static final String STR_ONE = "1";
    public static final String STR_TWO = "2";
    public static final String STR_THREE = "3";
    public static final String STR_FOUR = "4";

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final int FIVE = 5;

    /**
     * UTF-8字符集
     **/
    public static final String CHARSET_UTF8 = "UTF-8";

    public static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCKcAuwgGZSIR6g0oHb/7LKPt/rd6uWt8SUJWH6P4DYy7C//2BF7VH+GdtYXP3JMJsHG3zto9idPrF2Mx+/DNcFfN2YZtOp5yTRGYom5m7xnPRVK3plrUjc0DIrwkeyUc14fKl/aKHVf0sMWQP1wM8rMHuAibXqDN563XuPt3wP2QIDAQAB";

    public static final String PRIVATE_KEY = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAIpwC7CAZlIhHqDSgdv/sso+3+t3q5a3xJQlYfo/gNjLsL//YEXtUf4Z21hc/ckwmwcbfO2j2J0+sXYzH78M1wV83Zhm06nnJNEZiibmbvGc9FUremWtSNzQMivCR7JRzXh8qX9oodV/SwxZA/XAzyswe4CJteoM3nrde4+3fA/ZAgMBAAECgYA4T69RkkkMq1AsFrSf6nitb7nhrAHuAr5BjJs4EdC1XtV8o8Jjb09kztu8K8dHqS/GSlYczAL2o2PIMq99JRnpFmQRuGCmHVFFtU0ybt1Ujn59PsfO5hIzrH5ROSRjqhRb7zzCRtoOayeDIp2jmaWFJfqnNXTUWOUPPzQyiOTPyQJBAL0NMEVFbZBhBAnUpb7KtrWP6vO7X9PYypH+yurn4E6JI+jhJgvcXYMZBwUnIViJLvTI5be4hPEjvWMpF2uzdWsCQQC7dl3ouIXrGb018J+2HZ59a96TgOTL7NtOcwLjukYl/dfk7B2ifKBdPsVenFVE070qKg5iGAkwnMICCe4xUtzLAkEAkyhe+KzwekBqalSSqA8XZgEe/JZQI5FPLZHN1kike72YrEAF45mnWNL0efhZppcya+ytk/MX2LTfSP7FclqA/QJAVPcpSTmZOR3JbJFLUjfKhf5GO4TILsWEAOKQBLeA4+dR2yhJpkPmS10QB/nDjBaMPnghXMSYJbhnFpV5qFFuUQJBALZYYaru4FQ5i6ilRVQhckJBDyyDl5jh5JtI+cCFJ09kt2iCQX1W/aNZ0mhfz+MXYRyq8PAIK9qlg+VjhjzcPxM=";

    /**
     * Redis连接模式
     */
    public static final String STANDALONE = "single";
    public static final String CLUSTER = "cluster";
    public static final String SENTINEL = "sentinel";
}
