package com.xiaomi.mitv.mdm.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author liangjinxin
 */
public class JacksonUtil {
    /**
     * ObjectMapper 对象可重复使用
     */
    private static final ObjectMapper MAPPER;

    static {
        MAPPER = new ObjectMapper();
        // 忽略JSON字符串有的，而反序列化的Java对象没有的属性
        MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(JacksonUtil.class);

    private JacksonUtil() {
    }

    /**
     * 对象转换为jsonString
     *
     * @param obj 任意对象
     * @return json字符串
     */
    public static String toString(Object obj) {
        return toString(obj, "");
    }

    /**
     * 对象转换为jsonString
     *
     * @param obj           任意对象
     * @param defaultString 默认字符串
     * @return json字符串
     */
    public static String toString(Object obj, String defaultString) {
        try {
            return MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            LOGGER.error("writeValueAsString: {}", e.getMessage(), e);
        }
        return defaultString;
    }

    /**
     * 反序列化为对象
     *
     * @param jsonString json字符串
     * @param tClass     class对象
     * @param <T>        泛型
     * @return 指定类型的对象
     */
    public static <T> T toObject(String jsonString, Class<T> tClass) {
        return toObject(jsonString, null, tClass);
    }

    /**
     * 反序列化为对象
     *
     * @param jsonString   json字符串
     * @param defaultValue 默认值
     * @param clazz        class对象
     * @param <T>          泛型
     * @return 指定类型的对象
     */
    public static <T> T toObject(String jsonString, T defaultValue, Class<T> clazz) {
        try {
            return MAPPER.readValue(jsonString, clazz);
        } catch (Exception e) {
            LOGGER.error("readValue: {}", e.getMessage(), e);
        }
        return defaultValue;
    }

    /**
     * 反序列化为对象，带多重泛型
     *
     * @param jsonString    json字符串
     * @param typeReference 类型描述
     * @param <T>           泛型
     * @return 指定类型的对象
     */
    public static <T> T toObject(String jsonString, TypeReference<T> typeReference) {
        return toObject(jsonString, null, typeReference);
    }

    /**
     * 反序列化为对象，带多重泛型
     *
     * @param jsonString    json字符串
     * @param defaultValue  出错时的默认值
     * @param typeReference 类型描述
     * @param <T>           泛型
     * @return 指定类型的对象
     */
    public static <T> T toObject(String jsonString, T defaultValue, TypeReference<T> typeReference) {
        try {
            return MAPPER.readValue(jsonString, typeReference);
        } catch (Exception e) {
            LOGGER.error("readValue: {}", e.getMessage(), e);
        }
        return defaultValue;
    }

    /**
     * 反序列化对象，返回list
     *
     * @param jsonString json字符串
     * @param clazz      class对象
     * @param <T>        List中元素的泛型
     * @return 指定泛型的list
     */
    public static <T> List<T> ofList(String jsonString, Class<T> clazz) {

        if (StringUtil.isEmpty(jsonString)) {
            return Collections.emptyList();
        }
        CollectionType javaType = MAPPER.getTypeFactory().constructCollectionType(List.class, clazz);
        try {
            return MAPPER.readValue(jsonString, javaType);
        } catch (JsonProcessingException e) {
            LOGGER.error("ofList: {}", e.getMessage(), e);
        }
        return Collections.emptyList();
    }


    /**
     * 反序列化对象，返回map
     *
     * @param jsonString json字符串
     * @param keyClazz   key class对象
     * @param valueClazz value class对象
     * @param <K>        key 泛型
     * @param <V>        value 泛型
     * @return 指定key、value类型的map
     */
    public static <K, V> Map<K, V> ofMap(String jsonString, Class<K> keyClazz, Class<V> valueClazz) {
        if (StringUtil.isEmpty(jsonString)) {
            return null;
        }
        MapType javaType = MAPPER.getTypeFactory().constructMapType(Map.class, keyClazz, valueClazz);
        try {
            return MAPPER.readValue(jsonString, javaType);
        } catch (JsonProcessingException e) {
            LOGGER.error("ofMap: {}", e.getMessage(), e);
        }
        return null;
    }
}
