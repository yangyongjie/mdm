package com.xiaomi.mitv.mdm.common.result;

/**
 * @author liangjinxin
 */

public enum ResponseEnum {
    /**
     * 成功
     */
    SUCCESS(0, "success"),
    /**
     * 连接失败
     */
    CONNECT_FAIL(1, "connect fail"),
    /**
     * 参数错误
     */
    PARAM_ERROR(2, "param error"),
    /**
     * 签名错误
     */
    SIGN_ERROR(3, "sign error"),

    /**
     * key不存在
     */
    KEY_NOT_EXISTS(4, "key not exists"),

    /**
     * 连接工厂为空
     */
    FACTORY_NULL(5, "factory null"),
    /**
     * 未知错误
     */
    UNKNOWN_ERROR(9999, "unknown error");

    private final Integer code;

    private final String message;

    ResponseEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
